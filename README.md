CVE Service
===========

This container is part of CSTOOL.io - an open source vulnerability
search app. If you want to search for vulnerabilities, feel free to try
it out: 

# [http://CSTOOL.io](http://CSTOOL.io)


This service queries a database of [CVE and CPE](https://cve.mitre.org/) Items. This database is meant to be filled by the [cve-search](https://github.com/cve-search/cve-search) utility which queries the official NVD/Mitre database.

## License

Copyright 2018-2019 A. Koderman akoderman@koderman.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.