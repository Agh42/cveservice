cp prom/prometheus.yml /tmp
docker run -d \
--rm \
--name=prometheus \
-p 9090:9090 \
-v /tmp/prometheus.yml:/etc/prometheus/prometheus.yml \
prom/prometheus
#--config.file=/etc/prometheus/prometheus.yml
