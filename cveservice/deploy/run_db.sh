#!/bin/bash

#
# Run a local MongoDB like this:
#

docker run \
        --name cve-mongo \
        --network=cve-network \
        -v /home/docker/mongodb:/data/db   \
        -p 27017:27017 \
        -d mongo



