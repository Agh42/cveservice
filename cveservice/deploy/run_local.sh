# copy this file into a directory NOT commited to version control
# then change the env. variables to fit your development environment

docker run --rm -ti \
-e mongo_port=27017 \
-e mongo_host=cve-mongo \
-e mongo_user=user \
-e mongo_password=secret \
-e mongo_protocol=mongodb+srv \
-e mongo_database=cvedb \
akoderman/cveservice:latest
