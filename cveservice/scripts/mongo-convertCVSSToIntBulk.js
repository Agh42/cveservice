db = db.getSiblingDB("cvedb");
//print(db.getCollectionNames());
let bulk = [];
let count=1000

db.cves.find().forEach(
    function(myCve) {
      
---
###############################################################################
// Find matching documents
var docs = db.collection.aggregate([
    { $match: {
        test: { $exists: true }
    }},

    // Add a new field converting the decimal to a double
    // (alternatively, the original "test" value could also be replaced)
    { $addFields: {
        testDouble: { $toDouble: "$test" }
    }}
])

// Update with the changes (Note: this could be a bulk update for efficiency)
docs.forEach(function (doc) {
     db.collection.update({ _id: doc._id}, {$set: { testDouble: doc.testDouble }});
});

// Check the results
> db.collection.find().limit(1)
{
    "_id" : ObjectId("5d1a202e476381c30cd995a4"),
    "test" : NumberDecimal("0.1"),
    "testDouble" : 0.1
}
################################################################################
---



        bulk.push({
            "updateOne": {
                "filter": {_id: myCve._id},
                "update": { "$set": {
                    "cvssDouble": { "$toDouble": "$cvss"},
                }},
                "upsert": false
            }
        });

        if (bulk.length === 1000) {
            try {
                db.cves.bulkWrite(bulk);
                print(`Updated ${count} items`);
            }
            catch(e) { print(e); }
            bulk = [];
            count += 1000;
        }


    }
);
  

bulk.execute();
