db = db.getSiblingDB("cvedb");

result = db.cves.aggregate(
    { 
        $match: { 
           "vulnerable_product_stems": 'cpe:2.3:o:microsoft:windows_xp'
        } 
    },
    {
        $project : {
            "severity" : {
                $cond : [
                    {$gt : ["$cvss", 8.99]}, // if
                    "CRITICAL", // then
                    {$cond : [ // else
                        {$gt : ["$cvss", 6.99]}, // else if
                        "HIGH", // then
                        {$cond : [ //else
                            {$gt : ["$cvss", 3.99]}, // else if
                            "MEDIUM", // then
                            "LOW" // else
                        ]}
                    ]}
                ]
            },

        }
    },
    { 
        $group: {
            "_id" : {
                "severity" : "$severity",
            },
            "count" : {"$sum" : 1} 
        }
    }
);

result.forEach(printjson);
