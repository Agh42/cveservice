db = db.getSiblingDB("cvedb");

result = db.cves.aggregate(
    { 
        $match: { 
            "vulnerable_product_stems": 'cpe:2.3:o:linux:linux_kernel',
            "Published": {
                "$gte": new ISODate("2002-01-01T00:00:00Z"),
                "$lte": new ISODate("2020-01-12T00:00:00Z")
            }
        } 
    },
    {
        $project : {
            "severity" : {
                $cond : [
                    {$gt : ["$cvss", 8.99]}, // if
                    "CRITICAL", // then
                    {$cond : [ // else
                        {$gt : ["$cvss", 6.99]}, // else if
                        "HIGH", // then
                        {$cond : [ //else
                            {$gt : ["$cvss", 3.99]}, // else if
                            "MEDIUM", // then
                            "LOW" // else
                        ]}
                    ]}
                ]
            },
            "exploitTrueCount": { 
                $cond : ["$has_exploit", 1, 0]
            }

        }
    },
    { 
        $group: {
            "_id" : {
                "severity" : "$severity",
            },
            "count" : {"$sum" : 1},
            "exploitCount": {"$sum": "$exploitTrueCount"}
        }
    }
);

result.forEach(printjson);
