db = db.getSiblingDB("cvedb");
//print(db.getCollectionNames());
db.cpe.find().forEach(
    function(myCpe) {
        var product = myCpe.id.split(':')[4];
        var vendor =  myCpe.id.split(':')[3];
        //print( myCpe.id + "°" + product + "°" + vendor ); 
        db.cpe.update( {'_id': myCpe._id}, 
            {$set:{'product': product,
                'vendor': vendor} 
            }
        );
    }
);
