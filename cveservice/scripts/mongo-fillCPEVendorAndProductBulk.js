db = db.getSiblingDB("cvedb");
//print(db.getCollectionNames());
let bulk = [];
let count=1000

db.cpe.find().forEach(
    function(myCpe) {
        var product = myCpe.cpe_2_2.split(':')[4];
        var vendor =  myCpe.cpe_2_2.split(':')[3];
        //print( myCpe.id + "°" + product + "°" + vendor ); 
        
        bulk.push({
            "updateOne": {
                "filter": {_id: myCpe._id},
                "update": { "$set": {
                    'product': product,
                    'vendor': vendor
                }},
                "upsert": false
            }
        });

        if (bulk.length === 1000) {
            try {
                db.cpe.bulkWrite(bulk);
                print(`Updated ${count} items`);
            }
            catch(e) { print(e); }
            bulk = [];
            count += 1000;
        }


    }
);
