db = db.getSiblingDB("cvedb");
results = db.cves.aggregate(     
    { "$group": { "_id": "$id", "count": {"$sum": 1 } } },
    { "$match": { "_id": {"$ne":  null },
    "count": {"$gt": 1} }},
    { "$project": {"name": "$_id", "_id" : 0, "count": "$count"} } );

results.forEach(printjson);

