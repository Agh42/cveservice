db = db.getSiblingDB("cvedb");

result = db.cves.findOne(
    { 
        "vulnerable_product_stems": 'cpe:2.3:o:linux:linux_kernel',
        "Published": {
            "$gte": new ISODate("2001-01-01T00:00:00Z"),
            "$lte": new ISODate("2020-01-12T00:00:00Z")
        },
        "latestNews": {"$gte": new ISODate("2020-04-01T00:00:00Z")}
    },
    {"id": 1}
);

printjson(result);
