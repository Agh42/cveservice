db = db.getSiblingDB("cvedb");

//result = db.cves.aggregate(
var exp = db.cves.explain().aggregate(
    { 
        $match: { 
            //"vendors": "google",
            //"products": "chrome",
            "vulnerable_product_stems": "cpe:2\.3:a:google:chrome",
            "Published": {
                "$gte": new ISODate("2002-01-01T00:00:00Z"),
                "$lte": new ISODate("2020-04-07T00:00:00Z")
            }
        } 
    }, { 
        $sort : {
            "cvss" : -1,
            "Published": -1,
            "_id" : 1
        } 
    }, {
        $limit: 1000+50
    }, {
        $skip: 1000
    }, {
        $project : {
           "Published": 1,
           //"access": 1,
           "cvss": 1
           //"cwe": 1,
           //"impact": 1,
           //"vendors": 1,
           //"products" : 1
           //"summary": 1,
           //"vulnerable_product": 1,
           //"vulnerable_configuration": 1
        }
    }
);
printjson(exp);
//result.forEach(printjson);

