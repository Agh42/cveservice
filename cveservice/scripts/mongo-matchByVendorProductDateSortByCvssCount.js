db = db.getSiblingDB("cvedb");

result = db.cves.aggregate(
//var exp = db.cves.explain().aggregate(
    { 
        $match: { 
            //"vendors": "google",
            //"products": "chrome",
            "vulnerable_product_stems": "cpe:2\.3:a:google:chrome",
            "Published": {
                "$gte": new ISODate("2002-01-01T00:00:00Z"),
                "$lte": new ISODate("2020-04-07T00:00:00Z")
            }
        } 
    }, {
        $count: "totalCount"
    }
);
//printjson(exp);
result.forEach(printjson);

