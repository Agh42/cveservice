db = db.getSiblingDB("cvedb");
//print(db.getCollectionNames());

var result = db.cves.update(
    {
        "id": "CVE-2014-8872",
        $or: [
            {
                "latestNews": {
                    "$lte": new ISODate("2024-01-01T13:00:00Z")
                }
            },
            {
                "latestNews": null
            }
        ]
    },
    {
        $set: {
            "latestNews": new ISODate("2024-01-01T13:00:00Z")
        }
    }
);
printjson(result);
