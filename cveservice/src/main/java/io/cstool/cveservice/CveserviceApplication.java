package io.cstool.cveservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.cstool.cveservice.newsservice.repository.ArticleRepository;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
public class CveserviceApplication {

	@Autowired
	ArticleRepository newsRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(CveserviceApplication.class, args);
	}
}
