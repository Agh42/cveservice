package io.cstool.cveservice.account;

public class AccountException  extends RuntimeException {

	public AccountException(String msg) {
		super(msg);
	}

}
