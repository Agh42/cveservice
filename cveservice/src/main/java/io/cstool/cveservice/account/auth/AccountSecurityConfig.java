package io.cstool.cveservice.account.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@Configuration
@EnableWebSecurity
@Order(2)
public class AccountSecurityConfig extends WebSecurityConfigurerAdapter {

	// OIDC auth from client:
	@Value("${auth0.audience}")
	private String audience;

	@Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
	private String issuer;

	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception {
		// allow access to private paths only with OIDC token:
		httpSecurity
			.requestMatchers()
			// RBAC not supported on auth0.com's free tier:
			//.mvcMatchers("/**/accounts/**").hasAuthority("SCOPE_write:preferences") 
			.antMatchers("/**/accounts/**")

		.and()
			.cors() // makes spring security use the CORS config from Spring MVC
		.and()
			.csrf()
				.disable()
		.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.authorizeRequests()
				.anyRequest()
				.authenticated()
//			.mvcMatchers(HttpMethod.GET, "/**/inventories/**").hasAuthority("SCOPE_read:inventory")
//			.mvcMatchers(HttpMethod.DELETE, "/**/inventories/**").hasAuthority("SCOPE_write:inventory")
//			.mvcMatchers(HttpMethod.PATCH, "/**/inventories/**").hasAuthority("SCOPE_write:inventory")
//			.mvcMatchers(HttpMethod.POST, "/**/inventories/**").hasAuthority("SCOPE_write:inventory")
//			.mvcMatchers(HttpMethod.PUT, "/**/inventories/**").hasAuthority("SCOPE_write:inventory")
			
//			.mvcMatchers(HttpMethod.GET, "/**/inventories").authenticated()
//			.mvcMatchers(HttpMethod.DELETE, "/**/inventories").authenticated()
//			.mvcMatchers(HttpMethod.PATCH, "/**/inventories").authenticated()
//			.mvcMatchers(HttpMethod.POST, "/**/inventories").authenticated()
//			.mvcMatchers(HttpMethod.PUT, "/**/inventories").authenticated()
			
			.and()
				.oauth2ResourceServer().jwt();
	}

	@Bean
	JwtDecoder jwtDecoder() {
		NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder) JwtDecoders.fromOidcIssuerLocation(issuer);

		OAuth2TokenValidator<Jwt> audienceValidator = new AudienceValidator(audience);
		OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(issuer);
		OAuth2TokenValidator<Jwt> withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

		jwtDecoder.setJwtValidator(withAudience);

		return jwtDecoder;
	}

}
