package io.cstool.cveservice.account.controller;

import java.util.Set;

import javax.validation.Valid;

import com.auth0.exception.Auth0Exception;
import io.cstool.cveservice.account.AccountNotFoundException;
import io.cstool.cveservice.account.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.cstool.cveservice.account.entity.Account;
import io.cstool.cveservice.account.entity.Invite;
import io.cstool.cveservice.account.service.AccountService;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = { "http://cstool.io", "http://attacksrfc.cstool.io", "https://attacksrfc.cstool.io",
"http://localhost:3000" })
@RestController
@RequestMapping("/api/v1/accounts")
public class AccountsController {

	AccountService accountService;
	
	@Autowired
	AccountsController(AccountService accountService) {
		this.accountService = accountService;
	}

	@Value("${accountservice.claims.email}")
	private String emailClaim;

	@GetMapping(value="/me")
	public Account getAccountByExternalId(Authentication auth) {
		if (auth == null)
			throw new AccountNotFoundException();

		return accountService.findByExternalUserId(
				getLoggedInUserId(auth)
		);
	}
	
	@DeleteMapping(value="/me")
	public void deleteAccountByExternalId(Authentication auth) {
		accountService.delete(
				getLoggedInUserId(auth)
		);
	}
	
	@PutMapping(value="/me")
	public void saveBossAccount(@RequestBody @Valid Account account, Authentication auth) {
		account.setExternalUserId(getLoggedInUserId(auth));
		account.setEmail(getLoggedIdEmail(auth));
		accountService.saveAccount(account);
	}

	private UserInfo getUserInfo(Authentication auth) throws ResponseStatusException {
		Jwt token = (Jwt)auth.getPrincipal();
		try {
			return accountService.getUserInfo(token);
		} catch (Auth0Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Error while accessing authentication service.", e);
		}
	}

	/**
	 * Updates an existing user with additional details retrieved from the identification provider of the
	 * supplied authentication token.
	 *
	 * @param auth a valid authentication token
	 */
	@PutMapping(value="me/userinfo")
	public void saveUserInfo(Authentication auth) {
		var account = accountService.findByExternalUserId(getLoggedInUserId(auth));
		accountService.updateUserInfo(account, getUserInfo(auth));
	}

	@GetMapping(value="me/userinfo")
	public UserInfo findUserInfo(Authentication auth) {
		var account = accountService.findByExternalUserId(getLoggedInUserId(auth));
		return account.getUserInfo();
	}

	@PutMapping(value="me/voucher/{code}")
	public void submitVoucher(Authentication auth, @PathVariable String code) {
		accountService.redeemVoucher(getLoggedInUserId(auth), code);
	}

	/**
	 * Create a new invite.
	 *
	 * @param invite invite object containing the email
	 * @param auth
	 * @return
	 */
	@PostMapping(value="/me/tenant/invites")
	public Invite invite(@RequestBody @Valid Invite invite, Authentication auth) {
		return accountService.inviteUser(getLoggedInUserId(auth), invite);
	}

	@GetMapping(value="/me/invites")
	public Set<Invite> getInvites(Authentication auth) {
		return accountService.getInvitesFor(getLoggedInUserId(auth));
	}
	
	@GetMapping(value="/me/invites/{inviteId}")
	public Invite getInvite(Authentication auth, @RequestParam String inviteId) {
		return accountService.getInvite(getLoggedInUserId(auth), inviteId);
	}
	
	
	@PutMapping(value="/me/invites/{inviteId}/confirmation")
	public void acceptInvite(Authentication auth, @PathVariable String inviteId) {
		accountService.acceptInvite(getLoggedInUserId(auth), inviteId);
	}
	
	@DeleteMapping(value="/me/invites/{inviteId}/confirmation")
	public void rejectInvite(Authentication auth, @PathVariable String inviteId) {
		accountService.rejectInvite(getLoggedInUserId(auth), inviteId);
	}
	
	private String getLoggedInUserId(Authentication auth) {
		Jwt token = (Jwt)auth.getPrincipal();
		return token.getClaimAsString("sub");
	}

	private String getLoggedIdEmail(Authentication auth) {
		Jwt token = (Jwt)auth.getPrincipal();
		return token.getClaimAsString(emailClaim);
	}
}