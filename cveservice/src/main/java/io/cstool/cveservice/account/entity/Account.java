package io.cstool.cveservice.account.entity;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.cstool.cveservice.account.SubscriptionException;
import io.cstool.cveservice.account.event.AccountEvent.Type;
import io.cstool.cveservice.subscription.entity.Subscription;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.cstool.cveservice.account.event.AccountEvent;
import io.cstool.cveservice.account.value.Preferences;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.security.core.userdetails.UserDetails;

import static io.cstool.cveservice.account.event.AccountEvent.Type.*;

/**
 * Aggregate root for the user-accounts-aggregate.
 */
@Data
@NoArgsConstructor
@Valid
@Document
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Account extends AbstractAggregateRoot<Account> {

    public Account(String userId) {
        this.externalUserId = userId;
        this.createdOn = Instant.now();
    }

    @Id
    @EqualsAndHashCode.Include
    @JsonProperty(access = Access.READ_ONLY)
    private String id;

    @JsonProperty(access = Access.READ_ONLY)
    private Instant createdOn;


    @JsonProperty(access = Access.READ_ONLY)
    @Indexed(unique = true)
    private String externalUserId;

    @Version
    @JsonProperty(access = Access.READ_ONLY)
    private long version;

    @JsonProperty(access = Access.READ_ONLY)
    private String avatarUrl;

    @Email
    @JsonProperty(access = Access.READ_ONLY)
    @Indexed
    private String email;

    @JsonProperty(access = Access.READ_ONLY)
    private UserInfo userInfo;

    @Valid
    private Tenant tenant;

    @Valid
    @JsonProperty(access = Access.READ_ONLY)
    private Set<TenantRef> affiliations = new HashSet<>();

    @Valid
    private Preferences preferences;

    @Valid
    private Set<Inventory> inventories = new HashSet<>();

    public Account attachTenant(Tenant tenant) {
        if (this.tenant != null)
            throw new IllegalArgumentException("Cannot change tenant for account.");
        this.tenant = tenant;
        return this.andEvent(
                new AccountEvent(TENANT_CREATED, this.externalUserId, Instant.now()));
    }

    public Account remove() {
        tenant.remove();
        this.registerEvent(new AccountEvent(TENANT_DELETED, this.externalUserId, Instant.now(),
                tenant.getEntityId()));
        inventories.clear();
        return this.andEvent(
        		new AccountEvent(DELETED, this.externalUserId, Instant.now()));
    }

    public Account associateWith(TenantRef tenantRef) {
        affiliations.add(tenantRef);
        return this.andEvent(
				new AccountEvent(TENANT_ASSOCIATED, this.externalUserId, Instant.now()));
    }

    public Account disassociate(TenantRef tenantRef) {
        affiliations.remove(tenantRef);
        return this.andEvent(
				new AccountEvent(TENANT_DISASSOCIATED, this.externalUserId, Instant.now()));
    }


	public Invite invite(Account invitedAccount) {
    	registerEvent(new AccountEvent(NEW_INVITE, this.externalUserId, Instant.now()));
    	return getTenant().invite(invitedAccount);
	}

	public Account acceptInvite(String inviteId) {
		getTenant().acceptInvite(inviteId);
		return this.andEvent(new AccountEvent(INVITE_ACCEPTED, this.externalUserId, Instant.now()));
	}

	public Account rejectInvite(String inviteId) {
		getTenant().rejectInvite(inviteId);
		return this.andEvent(new AccountEvent(INVITE_REJECTED, this.externalUserId, Instant.now()));
	}

    public Account createSubscription(Subscription subscription) {
        getTenant().addSubscription(subscription);
        return this;
    }

    public void updateSubscription(Subscription subscription) {
        getTenant().updateSubscription(subscription);
    }

    public void cancelSubscription(Subscription subscription) {
        getTenant().cancelSubscription(subscription);
    }

    public void saveInventories(Set<Inventory> inventories) {
        long maxInv = tenant.getMaxInventories();
        long maxItems = tenant.getMaxItemsPerInventory();

        var itemCountOkay = inventories.stream()
                .filter(i -> i.getProducts().size() > maxItems)
                .collect(Collectors.toSet())
                .isEmpty();

        if (inventories.size()>maxInv || ! itemCountOkay)
            throw new SubscriptionException();

        this.inventories = inventories;
    }

    public Account updateAccount(Account potentiallyUnsafeUserInput) {
        setPreferences(potentiallyUnsafeUserInput.getPreferences());
        saveInventories(potentiallyUnsafeUserInput.getInventories());
        getTenant().setName(potentiallyUnsafeUserInput.getTenant().getName());
        return this.andEvent(new AccountEvent(UPDATED, this.externalUserId, Instant.now()));
    }

    public Account updateUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }
}