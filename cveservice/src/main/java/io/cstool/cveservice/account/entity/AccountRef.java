package io.cstool.cveservice.account.entity;

import lombok.Value;

@Value
public class AccountRef {
	private String id;
	
	public static AccountRef from(Account account) {
		return new AccountRef(account.getExternalUserId());
	}
	
	public boolean references(Account account) {
		return account.getId().equals(this.id);
	}
}
