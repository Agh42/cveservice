package io.cstool.cveservice.account.entity;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.cstool.cveservice.infrastructure.database.entity.Cpe;
import lombok.*;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * A named list of hardware and software products.
 */
@Value
@With
@Builder
@JsonDeserialize(builder = Inventory.InventoryBuilder.class)
@Valid
public class Inventory {

	@Size(max=255)
	private String name;

	@Size(max=1000)
	private List<Cpe> products;

	private boolean notify;

	@JsonPOJOBuilder(withPrefix = "")
	public static class InventoryBuilder {
		// required for Jackson
	}
}
