package io.cstool.cveservice.account.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

/**
 * Users can invite other users to their tenant.
 * This class represents open as well as accepted invites.
 * Invite is a value object within the accounts-aggregate.
 */
@Value
@With
@Builder
@Valid
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Invite {
	
	@EqualsAndHashCode.Include
	@JsonProperty(access = Access.READ_ONLY)
	private String entityId;
	
	@NotNull
	@NonNull
	@NotEmpty
	private String invitedEmail;
	
	@JsonProperty(access = Access.READ_ONLY)
	private boolean accepted;
	
	@JsonProperty(access = Access.READ_ONLY)
	private TenantRef tenantRef;
	
	public Invite accept() {
		return this.withAccepted(true);
	}
	
	public Invite reject() {
		return this.withAccepted(false);
	}

}
