package io.cstool.cveservice.account.entity;

import javax.validation.Valid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

import io.cstool.cveservice.subscription.entity.Subscription;
import org.springframework.data.annotation.Version;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * Entity within the accounts-aggregate.
 * Must only be accessed by the account aggregate root which represents the boss user owning a tenant.
 */
@Data
@Valid
public class Tenant {

    private Tenant() {
        // spring data constructor
    }

    public static Tenant existingTenant(UUID id) {
        var tenant = new Tenant();
        tenant.entityId = id.toString();
        return tenant;
    }

    public static Tenant newTenant(String name, Subscription defaultSubscription) {
        var tenant = new Tenant();
        tenant.entityId = UUID.randomUUID().toString();
        tenant.name = name;
        tenant.subscriptions.add(defaultSubscription);
        return tenant;
    }

    @EqualsAndHashCode.Include
    @JsonProperty(access = Access.READ_ONLY)
    private String entityId;

    @Version
    @JsonProperty(access = Access.READ_ONLY)
    private long version;

    @JsonProperty(access = Access.READ_ONLY)
    private AccountRef bossAccount;

    @NotNull
    @NonNull
    @NotEmpty
    private String name;

    @JsonProperty(access = Access.READ_ONLY)
    private Set<Invite> invites = new HashSet<>();

    @JsonProperty(access = Access.READ_ONLY)
    private Set<Subscription> subscriptions = new HashSet<>();

    public Invite invite(Account account) {
        var existingInvite = getInviteFor(account);
        if (existingInvite.isPresent())
            return existingInvite.get();

        var invite = Invite.builder()
                .entityId(UUID.randomUUID().toString())
                .accepted(false)
                .invitedEmail(account.getEmail())
                .tenantRef(TenantRef.from(this))
                .build();
        this.invites.add(invite);
        return invite;
    }

    public Optional<Invite> getInviteFor(Account account) {
        return invites.stream()
                .filter(i -> i.getInvitedEmail().equals(account.getEmail()))
                .findFirst();
    }

    public Account createBossAccount(String externalUserId, String email) {
        var account = new Account(externalUserId);
        account.attachTenant(this);
        account.setEmail(email);
        this.setBossAccount(AccountRef.from(account));
        return account;
    }

    public void acceptInvite(String inviteId) {
        rsvp(inviteId, true);
    }

    public void rejectInvite(String inviteId) {
        rsvp(inviteId, false);
    }

    private void rsvp(String inviteId, boolean accept) {
        var invite = getInvite(inviteId);
        invites.remove(invite);
        invites.add(accept ? invite.accept() : invite.reject());
    }

    public Invite getInvite(String inviteId) {
        return invites
                .stream()
                .filter(i -> i.getEntityId().equals(inviteId))
                .findFirst()
                .orElseThrow();
    }

    public void addSubscription(Subscription subscription) {
        subscriptions.add(subscription);
    }

    public void removeSubscription(Subscription subscription) {
        cancelSubscription(subscription);
        subscriptions.remove(subscription);
    }

    public Subscription cancelSubscription(Subscription subscription) {
        var cancelledSubscription = subscriptions.stream()
                .filter(s -> s.equals(subscription))
                .findFirst()
                .orElseThrow()
                .cancel();
        subscriptions.remove(subscription);
        subscriptions.add(cancelledSubscription);
        return cancelledSubscription;
    }

    public void remove() {
        subscriptions.forEach(Subscription::cancel);
        subscriptions.clear();
    }

    public void updateSubscription(Subscription subscription) {
        var oldSubscription = subscriptions.stream()
                .filter(s -> s.equals(subscription))
                .findFirst()
                .orElseThrow();
        subscriptions.remove(oldSubscription);
        subscriptions.add(subscription);
    }

    public Subscription getSubscription(String id) {
        return subscriptions.stream().filter(s -> s.getId().equals(id))
                .findFirst().orElseThrow();
    }

    public long getMaxInventories() {
        return subscriptions.stream()
                .mapToLong(Subscription::getMaxInventories)
                .max()
                .orElseThrow();
    }

    public long getMaxItemsPerInventory() {
        return subscriptions.stream()
                .mapToLong(Subscription::getMaxProductsPerInventory)
                .max()
                .orElseThrow();
    }
}
