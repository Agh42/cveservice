package io.cstool.cveservice.account.entity;

import lombok.Value;

@Value
public class TenantRef {
	private String id;
	
	public static TenantRef from(Tenant tenant) {
		return new TenantRef(tenant.getEntityId());
	}
	
	public boolean references(Tenant tenant) {
		return tenant.getEntityId().equals(this.id);
	}
}
