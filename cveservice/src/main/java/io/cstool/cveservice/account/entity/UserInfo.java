package io.cstool.cveservice.account.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.With;

import javax.validation.Valid;
import java.util.Date;

@Value
@Builder
public class UserInfo {
    boolean emailVerified;
    String nickname;
    String pictureUrl;
    String sub;
    Date updatedAt;
}
