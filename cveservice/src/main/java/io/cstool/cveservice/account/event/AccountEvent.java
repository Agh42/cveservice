package io.cstool.cveservice.account.event;

import java.time.Instant;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.lang.Nullable;

@Value
public class AccountEvent {

	public enum Type {
		TENANT_CREATED,
		TENANT_ASSOCIATED,
		TENANT_DISASSOCIATED,
		TENANT_DELETED,
		NEW_INVITE,
		INVITE_ACCEPTED,
		INVITE_REJECTED,
		DELETED,
		UPDATED
	}
	
	Type type;
	String externalAccountId;
	Instant timestamp;
	String tenantId;

	public AccountEvent(Type type, String externalAccountId, Instant timestamp, String tenantId) {
		this.type = type;
		this.externalAccountId = externalAccountId;
		this.timestamp = timestamp;
		this.tenantId = tenantId;
	}

	public AccountEvent(Type type, String externalAccountId, Instant timestamp) {
		this(type, externalAccountId, timestamp, null);
	}
}
