package io.cstool.cveservice.account.repository;

import java.util.Set;

import io.cstool.cveservice.account.entity.TenantRef;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import io.cstool.cveservice.account.entity.Account;
import io.cstool.cveservice.account.entity.Invite;
import lombok.NonNull;

	
//@RepositoryRestResource(collectionResourceRel = "accounts", path = "accounts")
//@CrossOrigin(origins={"http://cstool.io", "http://attacksrfc.cstool.io",
//		"https://attacksrfc.cstool.io", "http://localhost:3000"})
@RestResource(exported = false)
public interface AccountRepository extends MongoRepository<Account, String> {

	public Account findByExternalUserId(String externalUserId);

	public Account findByEmail(String email);

	/**
	 * Returns boss accounts that invited the email to their tenant.
	 * 
	 * @param email the invited email address
	 * @return
	 */
	public Set<Account> findByTenantInvitesInvitedEmail(String email);

	public Set<Account> findByTenantSubscriptionsId(String id);

	/**
	 * Returns boss account whose tenant contains the given invite.
	 * 
	 * @param inviteId the id of the searched invite
	 * @return
	 */
	public Account findByTenantInvitesEntityId(String inviteId);

	/**
	 * Find accounts affiliated with any of the given tenants.
	 */
	public Set<Account> findByAffiliationsIn(Set<TenantRef> affiliations);

}
