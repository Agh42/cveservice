package io.cstool.cveservice.account.service;

import com.auth0.client.auth.AuthAPI;
import com.auth0.exception.Auth0Exception;
import io.cstool.cveservice.account.AccountException;
import io.cstool.cveservice.account.AccountNotFoundException;
import io.cstool.cveservice.account.entity.*;
import io.cstool.cveservice.account.event.AccountEvent;
import io.cstool.cveservice.account.repository.AccountRepository;
import io.cstool.cveservice.account.value.Preferences;
import io.cstool.cveservice.subscription.entity.Subscription;
import io.cstool.cveservice.subscription.repository.VoucherRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.cstool.cveservice.account.event.AccountEvent.Type.TENANT_DELETED;
import static java.util.Collections.singleton;

/**
 * Service containing use cases for account management. Retrieves aggregates and resolves entities using the account
 * repository before calling methods on the domain objects.
 */
@Service
@Slf4j
public class AccountService {

    private final AuthAPI auth0;

    private final AccountRepository accountRepo;

    private final VoucherRepository voucherRepo;

    @Value("${subscription.defaultMaxInventories}")
    private long defaultMaxInventories;

    @Value("${subscription.defaultMaxProductsPerInventory}")
    private long defaultMaxProductsPerInventory;

    @Value("${subscription.defaultTitle}")
    private String defaultTitle;

    AccountService(AccountRepository repo,
                   VoucherRepository voucherRepo,
                   @Value("${auth0.domain}") String auth0Domain,
                   @Value("${auth0.clientid}") String auth0ClientId,
                   @Value("${auth0.clientSecret}") String auth0ClientSecret
    ) {
        this.accountRepo = repo;
        this.voucherRepo = voucherRepo;
        this.auth0 = new AuthAPI(auth0Domain, auth0ClientId, auth0ClientSecret);
    }

    @Transactional
    public Account createBossAccount(@NonNull String userId, String email, UserInfo userInfo) {
        var existingAccount = accountRepo.findByExternalUserId(userId);
        if (existingAccount != null)
            return existingAccount;
        var account = Tenant
                .newTenant("My organization", defaultSubscription(email))
                .createBossAccount(userId, email);
        account.setPreferences(Preferences.builder()
                .notificationsHotTopics(true)
                .notificationsInventories(true)
                .build());
        account.setUserInfo(userInfo);
        return accountRepo.save(account);
    }

    private Subscription defaultSubscription(String email) {
        return Subscription.builder()
                .name(defaultTitle)
                .maxInventories(defaultMaxInventories)
                .maxProductsPerInventory(defaultMaxProductsPerInventory)
                .validUntil(Instant.parse("3000-01-01T00:00:00Z"))
                .createdOn(Instant.now())
                .id(UUID.randomUUID().toString())
                .email(email)
                .build();
    }

    public Account saveAccount(Account account) {
        var existingAccount = accountRepo.findByExternalUserId(account.getExternalUserId());
        if (existingAccount == null)
            return createBossAccount(account.getExternalUserId(),
                    account.getEmail(),
                    account.getUserInfo());
        return accountRepo.save(existingAccount.updateAccount(account));
    }

    public Account findByExternalUserId(@NotBlank @NotNull String externalId) {
        var account = accountRepo.findByExternalUserId(externalId);
        if (account == null)
            throw new AccountNotFoundException();
        return account;
    }

    @TransactionalEventListener
    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void handleAccountEvent(AccountEvent event) {
        log.info("Received event. Type: {}, Account: {}, Tenant: {}, Time: {}",
                event.getType(),
                event.getExternalAccountId(),
                event.getTenantId(),
                event.getTimestamp());

        if (event.getType().equals(TENANT_DELETED)) {
            var tenantRef = new TenantRef(event.getTenantId());
            var accounts = accountRepo.findByAffiliationsIn(
                    singleton(tenantRef)
            );
            // We update all accounts in the single current transaction.
            // tThis could be moved to a separate business method
            // to create a new logical TA for each account aggregate:
            accounts.forEach(account -> {
                accountRepo.save(account.disassociate(tenantRef));
            });
        }
    }


    public void delete(@NotBlank @NotNull String externalId) {
        var account = findByExternalUserId(externalId);
        account = accountRepo.save(account.remove()); // save only required to publish event
        accountRepo.delete(account); // events on delete: supported from 2020.0.0, can remove the save then
    }

    public Invite inviteUser(String currentBossUserId, @Valid Invite invite) {
        var currentBoss = accountRepo.findByExternalUserId(currentBossUserId);
        if (currentBoss == null)
            throw new AccountNotFoundException();

        var invitedAccount = accountRepo.findByEmail(invite.getInvitedEmail());
        if (invitedAccount == null)
            throw new AccountNotFoundException();

        invite = currentBoss.invite(invitedAccount);
        accountRepo.save(currentBoss);
        return invite;
    }

    /**
     * Get invites from all tenants for this user.
     */
    public Set<Invite> getInvitesFor(String userId) {
        var email = accountRepo.findByExternalUserId(userId)
                .getEmail();
        // find boss accounts that have invited the user to their tenant:
        var bossAccounts = accountRepo.findByTenantInvitesInvitedEmail(email);

        // filter invites for the user:
        return bossAccounts.stream()
                .map(Account::getTenant)
                .flatMap(t -> t.getInvites()
                        .stream())
                .filter(i -> i.getInvitedEmail()
                        .equals(email))
                .collect(Collectors.toSet());
    }

    /**
     * Get a specific invite.
     */
    public Invite getInvite(String currentUserId, String inviteId) {
        return getInvitesFor(currentUserId).stream()
                .filter(i -> i.getEntityId()
                        .equals(inviteId))
                .findFirst()
                .orElseThrow();
    }

    public void acceptInvite(String currentUserId, String inviteId) {
        var boss = accountRepo.findByTenantInvitesEntityId(inviteId);
        var currentUser = accountRepo.findByExternalUserId(currentUserId);
        var invite = boss.getTenant().getInvite(inviteId);
        validateEmail(currentUser, invite);

        accountRepo.save(
                currentUser.associateWith(
                        TenantRef.from(boss.getTenant())
                )
        );
        accountRepo.save(boss.acceptInvite(inviteId));
    }

    public void rejectInvite(String userId, String inviteId) {
        var boss = accountRepo.findByTenantInvitesEntityId(inviteId);
        var currentUser = accountRepo.findByExternalUserId(userId);
        var invite = boss.getTenant().getInvite(inviteId);
        validateEmail(currentUser, invite);

        accountRepo.save(
                currentUser.disassociate(
                        TenantRef.from(boss.getTenant())
                )
        );
        accountRepo.save(boss.rejectInvite(inviteId));
    }

    private void validateEmail(Account currentUser, Invite invite) {
        if (!invite.getInvitedEmail().equals(currentUser.getEmail()))
            throw new AccountNotFoundException();
    }

    public void cancelSubscription(String id) {
        var account = accountRepo.findByTenantSubscriptionsId(id)
                .stream().findFirst().orElseThrow();
        account.cancelSubscription(
                account.getTenant().getSubscription(id)
        );
        accountRepo.save(account);
    }

    public void updateSubscription(String subscriptionId, Subscription subscription) {
        var account = accountRepo
                .findByEmail(subscription.getEmail());
        account.updateSubscription(subscription);
        accountRepo.save(account);
    }

    public void createSubscription(Subscription subscription) {
        var account = accountRepo
                .findByEmail(subscription.getEmail())
                .createSubscription(subscription);
        accountRepo.save(account);

    }

    public UserInfo getUserInfo(Jwt token) throws Auth0Exception {
        var infoRequest = auth0.userInfo(token.getTokenValue());
        var info = infoRequest.execute().getValues();
        return UserInfo.builder()
                .emailVerified((Boolean) info.get("email_verified"))
                .nickname(String.valueOf(info.get("nickname")))
                .updatedAt(Date.from(Instant.parse(String.valueOf(info.get("updated_at")))))
                .pictureUrl(String.valueOf(info.get("picture")))
                .sub(String.valueOf(info.get("sub")))
                .build();
    }

    public void updateUserInfo(Account account, UserInfo userInfo) {
        var existingAccount = accountRepo.findByExternalUserId(account.getExternalUserId());
        accountRepo.save(existingAccount.updateUserInfo(userInfo));
    }

    @Transactional
    public void redeemVoucher(@NonNull @NotBlank String userId, @NonNull @NotBlank String code) {
        var currentUser = accountRepo.findByExternalUserId(userId);
        var voucher = voucherRepo.findFirstByCodeIgnoreCase(code).orElseThrow();
        // do not use same code twice:
        if (currentUser.getTenant().getSubscriptions().stream()
                .anyMatch(subscription -> subscription.getUsedVoucherCode().orElse("").equalsIgnoreCase(code)))
            throw new AccountException("This voucher code was already used for this account.");

        accountRepo.save(
                currentUser.createSubscription(
                        voucher.redeem(currentUser.getEmail())
                )
        );
        // save voucher with increased usage count:
        voucherRepo.save(voucher);
    }
}