package io.cstool.cveservice.account.value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Value
@With
@Valid
@Builder
public class Preferences {
	
	private boolean notificationsHotTopics;
	
	private boolean notificationsInventories;
	
}
