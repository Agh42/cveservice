package io.cstool.cveservice.adapters.controller.event;

import io.cstool.cveservice.eventstore.service.EventStore;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;
import java.util.Set;

/**
 * Uses a timer bean to poll the CVE database and create change events.
 * Simulates events that are not currently created because the
 * database is populated by the external cve-search tool which
 * does not produce events as of now.
 */
@Component
public class CveEventProducer {

    @Autowired
    MongoGateway mongoGateway;

    @Autowired
    EventStore eventStore;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Scheduled(cron="${cveservice.cve_summary_events.cron}")
    @Transactional
    public void produceEvents() {
        var lastSummaryEvent = eventStore.findNewestEvent(CveSummaryEvent.KEY);

        Instant lastPublicationDate;
        if (lastSummaryEvent.isEmpty()
                || !(lastSummaryEvent.get().getType().equals(CveSummaryEvent.KEY))) {
            // start at current time:
            lastPublicationDate = Instant.now();
        }
        else {
            lastPublicationDate = lastSummaryEvent.get()
                    .getOcurredOn().toInstant();
        }

        // get changed CVEs since last published summary:
        var newCves = mongoGateway.findNewCvesSince(lastPublicationDate);

        // don't publish event if no new CVEs:
        if (newCves == null || newCves.isEmpty())
            return;

        // publish cve summary event with changed CVEs - or just the ID of the last
        // known (newest) CVE if this is the first event in a new database:
        publisher.publishEvent(new CveSummaryEvent(
                Set.copyOf(newCves),
                new Date(),
                mongoGateway.findNewestCveDbId()
        ));
    }
}
