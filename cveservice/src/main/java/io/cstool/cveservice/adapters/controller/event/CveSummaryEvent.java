package io.cstool.cveservice.adapters.controller.event;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import io.cstool.cveservice.infrastructure.database.entity.Cve;
import io.cstool.cveservice.infrastructure.database.entity.CveProductProjection;
import lombok.Data;
import lombok.NonNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNullElse;


/**
 * Represents a notification about newly found CVEs. Contains a list of all CVEs
 * that were newly added between this event and its immediate predecessor.
 *
 * Also contains the last known CVE ID. By requesting the latest CveSummaryEvent
 * from the EventStore, it is possible to determine which CVEs are
 * newer and have not been processed for notification.
 */
@Data
public class CveSummaryEvent implements DomainEvent {

    public static final String KEY = "cveservice.cvesummarypublished";

    public static final String LAST_KNOWN_CVE_DBID = "lastKnownCveDbId";

    public static final String AFFECTED_PRODUCTS = "affectedProducts";

    /**
     * All CVEs mentioned since the last products that are affected by the CVEs mentioned in {@code newCves}
     * {
     *     CVE-2020-1234: [
     *         o:microsoft:windows,
     *         a:microsoft:office365
     *     ],
     *     CVE-2020-5678: [
     *      o:linux:linux_kernel
     *     ]
     *     ...
     * }
     */
    private final Map<String, List<String>> affectedProducts;

    private String id = UUID.randomUUID().toString();


    /**
     * The CVE-Ids (i.e. CVE-2020-1234) of all CVEs that were added between this event and its predecessor.
     */
    @NonNull
    private Set<String> newCves;

    @NonNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date published;


    /**
     * The newest known CVE database ID at the time that this event was published.
     */
    @NonNull
    private String lastKnownCveDbId;

    public CveSummaryEvent(@NonNull Set<CveProductProjection> newCves,
                           @NonNull Date published, String lastKnownCveDbId) {
        this.newCves = newCves.stream().map(CveProductProjection::getCveId).collect(Collectors.toSet());
        this.affectedProducts = mapProducts(newCves);
        this.published = published;
        this.lastKnownCveDbId = requireNonNullElse(lastKnownCveDbId, "");
    }

    private Map<String, List<String>> mapProducts(@NonNull Set<CveProductProjection> newCves) {
        return newCves.stream().collect(Collectors.toMap(CveProductProjection::getCveId,
                CveProductProjection::getAffectedProducts));
    }

    @Override
    public Set<String> getAffectedCves() {
        return this.newCves;
    }

    @Override
    public String getRoutingKey() {
        return KEY;
    }

    @Override
    public Map<String, Serializable> getData() {
        return Collections.emptyMap();
    }
}



