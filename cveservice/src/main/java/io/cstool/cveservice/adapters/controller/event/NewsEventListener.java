package io.cstool.cveservice.adapters.controller.event;

import io.cstool.cveservice.service.CveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.cstool.cveservice.newsservice.event.NewsServiceEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * Handles events received from the news service.
 * The reference to the NewsServiceEvent could be removed
 * and event handling switched to RabbitMQ to separate the services
 * into independently deployable units.
 *
 */
@Component
@Slf4j
public class NewsEventListener{

	@Autowired
	private CveService cveService;

	// begin new thread and session to not suspend the publisher's transaction.
	// Called after publisher's successful commit.
    @TransactionalEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Async
    void handleNewsServiceEvent(NewsServiceEvent event) {
    	log.info("Event received for CVEs {}: type {}, published {}, id {} ",
    			event.getCvesMentioned(), 
    			event.getEventType(), 
    			event.getPublished(), 
    			event.getId());

    	cveService.updateLatestNews(event.getCvesMentioned(), event.getPublished().toInstant());
    }
}