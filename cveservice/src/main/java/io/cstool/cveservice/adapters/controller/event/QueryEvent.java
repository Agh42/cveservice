package io.cstool.cveservice.adapters.controller.event;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class QueryEvent {

	@NonNull
    private String name;
    
	@NonNull
    private SearchType searchType;
}