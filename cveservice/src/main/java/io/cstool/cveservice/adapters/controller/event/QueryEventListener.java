package io.cstool.cveservice.adapters.controller.event;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import io.cstool.cveservice.newsservice.event.NewsServiceEvent;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class QueryEventListener {

    @Async
    @TransactionalEventListener
    // no @Transactional required because no DB read/write operation is taking place
    @EventListener(condition = "#event.searchType=SearchType.PAGINATED")
    void handleQuery(QueryEvent event){
    	log.info("Paginated query event triggered for element: {} ", event.getName());
    }
}