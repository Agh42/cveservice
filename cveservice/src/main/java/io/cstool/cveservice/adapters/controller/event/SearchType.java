package io.cstool.cveservice.adapters.controller.event;

public enum SearchType {
	PAGINATED,
	UNLIMITED
}
