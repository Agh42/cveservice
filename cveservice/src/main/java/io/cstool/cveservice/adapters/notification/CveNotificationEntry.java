package io.cstool.cveservice.adapters.notification;

import lombok.*;

import java.util.List;

@Value
@With
@Builder
public class CveNotificationEntry {

    String vulnerability;
    String cvssv2;
    String cvssv3;
    String summary;
    boolean hasExploit;

    @Singular
    List<String> affectedInventories;

    @Singular
    List<String> affectedProducts;

    public String getAffectedProductsJoined() {
        return String.join(", ", affectedProducts);
    }

    public String getAffectedInventoriesJoined() {
        return String.join(", ", affectedInventories);
    }
}
