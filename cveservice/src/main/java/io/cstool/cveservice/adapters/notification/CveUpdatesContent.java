package io.cstool.cveservice.adapters.notification;

import io.cstool.cveservice.notification.entity.Content;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class CveUpdatesContent implements Content {

    public static final String TEMPLATE = "mail_inventory";

    private Map<String, Object> model = new HashMap<>();

    private Locale locale;

    @Override
    public String getTemplateName() {
        return TEMPLATE;
    }

    public CveUpdatesContent(Locale locale,
                          String recipientMail,
                          List<CveNotificationEntry> cveNotificationEntries) {
        this.locale = locale;
        model.put("recipientMail", recipientMail);
        model.put("notifications", cveNotificationEntries);
    }

    public boolean hasAffectedInventories() {
        return ((List<CveNotificationEntry>) model.get("notifications"))
                .stream().anyMatch(e -> e.getAffectedInventories().size()>0);
    }

    public List<CveNotificationEntry> getEntriesWithAffectedInventories() {
        return ((List<CveNotificationEntry>) model.get("notifications"))
                .stream().filter(e -> e.getAffectedInventories().size()>0)
                .collect(Collectors.toList());
    }

}
