package io.cstool.cveservice.eventstore.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public interface DomainEvent extends Serializable {

    String getId();

    /**
     * The CVE IDs that were affected by this event.
     *
     */
    Set<String> getAffectedCves();

    Date getPublished();

    /**
     * The routing key is used by a message queue to determine which queues
     * will receive the event message.
     *
     * I.e.: "cveservice.cveupdated"
     *
     */
    String getRoutingKey();

    /**
     * Additional arbitrary event information.
     * @return
     */
    Map<String, Serializable> getData();
}
