package io.cstool.cveservice.eventstore.entity;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.DBObject;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Data
@Valid
@Document
/**
 * Stores a domain event.
 */
public class StoredEvent {

    @Id
    private String id;

    @NonNull
    @NotNull
    @PastOrPresent(message = "Invalid occurence date")
    @Indexed
    @CreatedDate
    private Date ocurredOn;

    @NonNull
    @NotNull
    @NotBlank(message = "Missing type")
    private String type;

    @NonNull
    @NotNull
    private Set<String> affectedCves;

    private DBObject originalEvent;

    /**
     * Arbitrary additional event information.
     */
    private Map<String, Serializable> eventData;
}
