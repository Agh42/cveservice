package io.cstool.cveservice.eventstore.listener;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import io.cstool.cveservice.eventstore.service.EventStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * Stores a domain event in the event store. Uses the existing transaction to make sure
 * that the event store is consistent with state changes.
 *
 * Creates its own transaction to save the received event if no existing
 * transaction was found.
 *
 */
@Component
public class EventStoringListener {

    @Autowired
    private EventStore eventStore;

    @TransactionalEventListener(fallbackExecution = true)
    @Transactional
    void handleDomainEvent(DomainEvent event) {
        eventStore.append(event);
    }
}
