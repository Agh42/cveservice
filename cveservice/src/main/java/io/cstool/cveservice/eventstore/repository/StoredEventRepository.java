package io.cstool.cveservice.eventstore.repository;

import io.cstool.cveservice.eventstore.entity.StoredEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface StoredEventRepository extends MongoRepository<StoredEvent, String> {

    Optional<StoredEvent> findTopByTypeOrderByIdDesc(String key);

    List<StoredEvent> findByTypeAndOcurredOnAfterOrderByIdDesc(String key, Date from);
}
