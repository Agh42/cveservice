package io.cstool.cveservice.eventstore.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import io.cstool.cveservice.eventstore.entity.DomainEvent;
import io.cstool.cveservice.eventstore.entity.StoredEvent;
import org.springframework.stereotype.Service;

@Service
public class EventSerializer {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private DBObject serialize(DomainEvent event) {
        return BasicDBObject.parse(objectMapper.valueToTree(event).toString());
    }

    public StoredEvent write(DomainEvent domainEvent) {
        var event = new StoredEvent(domainEvent.getPublished(),
                domainEvent.getRoutingKey(),
                domainEvent.getAffectedCves());
        event.setEventData(domainEvent.getData());
        event.setOriginalEvent(serialize(domainEvent));
        return event;
    }


}
