package io.cstool.cveservice.eventstore.service;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import io.cstool.cveservice.eventstore.entity.StoredEvent;
import io.cstool.cveservice.eventstore.repository.StoredEventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class EventStore {

    @Autowired
    private StoredEventRepository repository;

    @Autowired
    private EventSerializer serializer;

    @Transactional
    public StoredEvent append(DomainEvent domainEvent) {
        log.info("Storing event: Key:{} / Id:{}", domainEvent.getRoutingKey(), domainEvent.getId());
        return repository.save(serializer.write(domainEvent));
    }

    /**
     * Find the newest stored event that was published of the given type.
     *
     * @param key the routing key of the wanted event type
     */
    @Transactional
    public Optional<StoredEvent> findNewestEvent(String key) {
        return repository.findTopByTypeOrderByIdDesc(key);
    }

    @Transactional
    public List<StoredEvent> findEventsNewerThan(String key, Date from) {
        return repository.findByTypeAndOcurredOnAfterOrderByIdDesc(key, from);
    }

}
