package io.cstool.cveservice.infrastructure.database.access;

import java.time.Instant;

import org.bson.Document;
import org.bson.conversions.Bson;

import io.cstool.cveservice.infrastructure.database.entity.TimeRange;

public class CveNewsQuery {

	private static final String QUERY_HAS_NEWS = " { " + 
			"        \"vulnerable_product_stems\": '%s'," + 
			"        \"Published\": {" + 
			"            \"$gte\": new ISODate(\"%s\")," + 
			"            \"$lte\": new ISODate(\"%s\")" + 
			"        }," + 
			"        \"latestNews\": {$ne:null}" + 
			"    }";
	
	private static final String QUERY_HAS_CURRENT_NEWS = " { " + 
			"        \"vulnerable_product_stems\": '%s'," + 
			"        \"Published\": {" + 
			"            \"$gte\": new ISODate(\"%s\")," + 
			"            \"$lte\": new ISODate(\"%s\")" + 
			"        },\n" + 
			"        \"latestNews\": {\"$gte\": new ISODate(\"%s\")}" + 
			"    }";
	
	public Bson hasNews(String cpeId, TimeRange timerange) {
		return Document.parse(
				String.format(QUERY_HAS_NEWS,
				cpeId,
				timerange.getFrom().toString(), 
				timerange.getUntil().toString()
		));
	}
	
	public Bson hasCurrentNews(String cpeId, TimeRange timerange, Instant recentNewsCutoff) {
		return Document.parse(
				String.format(QUERY_HAS_CURRENT_NEWS,
				cpeId,
				timerange.getFrom().toString(), 
				timerange.getUntil().toString(),
				recentNewsCutoff.toString()
		));
	}
	
}
