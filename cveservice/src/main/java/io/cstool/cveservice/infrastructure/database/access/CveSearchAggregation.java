package io.cstool.cveservice.infrastructure.database.access;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.bson.Document;

import io.cstool.cveservice.infrastructure.database.entity.Cpe;
import io.cstool.cveservice.infrastructure.database.entity.TimeRange;

public final class CveSearchAggregation {
	
	public static final int SORT_ASC = 1;
	public static final int SORT_DESC = -1;
	public static final String FIELD_COUNT = "totalCount";
	
	private static String AGGREGATION_MATCH_WITH_TIMERANGE = " { " + 
			"        $match: { " + 
			"            \"vulnerable_product_stems\": \"%s\"," + 
			"            \"Published\": {" + 
			"                \"$gte\": new ISODate(\"%s\")," + 
			"                \"$lte\": new ISODate(\"%s\")" + 
			"            }" + 
			"        } " + 
			"    }";
	
	private static String AGGREGATION_MATCH_WITHOUT_TIMERANGE = " { " + 
			"        $match: { " + 
			"            \"vulnerable_product_stems\": \"%s\" " + 
			"        } " + 
			"    }";
	
	
	private static String AGGREGATION_LIMIT = "{" + 
			"        $limit: %s" + // must be skip + limit!
			"    }";
	
	private static String AGGREGATION_SKIP = "{" + 
			"        $skip: %s" + 
			"    }";
	
	private static String AGGREGATION_SORT = "{ " + 
			"        $sort : {" + 
			"            \"has_exploit\": -1," + 
			"            \"latestNews\":  -1," + 
			"            \"%s\" : %s," + 
			"            \"Published\": -1," + // TODO variable sort keys
			"            \"_id\" : 1" + // id guarantees deterministic sort order when other keys are equal
			"        } " + 
			"    }";
	
	private static String AGGREGATION_PROJECT = "{" + 
			"        $project : {" + 
			"           %s" +
			"        }" + 
			"    }";

	private static String AGGREGATION_COUNT = "{" + 
			"        $count: \"" + FIELD_COUNT + "\"" + 
			"    }";
	
	
	/**
	 * Create an aggregation query document with sorting and paginated results.
	 * 
	 * @param vendor
	 * @param product
	 * @param itemsPerPage
	 * @param requestedPage
	 * @param fields
	 * @param sortBy
	 * @param sortOrder
	 * @param timeRange
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static List<Document> cvesForCpesAggregationStages(
			Cpe cpe,
			Integer itemsPerPage,
			Integer requestedPage,
			String[] fields,
			String sortBy,
			int sortOrder,
			Optional<TimeRange> timeRange) throws UnsupportedEncodingException {
		
		int skip = itemsPerPage * (requestedPage-1);
			
		Document aggregationMatch = aggregationMatch(cpe.getCpe_2_2(), timeRange);
		
		Document aggregationSort = Document.parse(
				String.format(AGGREGATION_SORT,
						sortBy,
						sortOrder)
		);
		
		Document aggregationLimit = Document.parse(
				String.format(AGGREGATION_LIMIT, 
						skip + itemsPerPage) // i.e. for third page of 50 items: 100+50
		);
		
		Document aggregationSkip = Document.parse(
				String.format(AGGREGATION_SKIP, 
						skip) // i.e. for third page of 50 items: 100
		);
		
		Document aggregationProject = Document.parse(
				String.format(AGGREGATION_PROJECT,
						joinFields(fields))
		);
		
		return Arrays.asList(aggregationMatch,
				aggregationSort,
				aggregationLimit,
				aggregationSkip,
				aggregationProject);
	}

	private static Document aggregationMatch(String cpe, Optional<TimeRange> timeRange) {
		Document aggregationMatch;
		if (timeRange.isEmpty()) {
			aggregationMatch = Document.parse(
					String.format(AGGREGATION_MATCH_WITHOUT_TIMERANGE,
							cpe
					)
			);
		}
		else {
			aggregationMatch = Document.parse(
					String.format(AGGREGATION_MATCH_WITH_TIMERANGE,
							cpe,
							timeRange.get().getFrom().toString(), 
							timeRange.get().getUntil().toString()
					)
			);
		}
		return aggregationMatch;
	}
	
	/**
	 * Aggregation to return the total count of items matching the query.
	 * 
	 * @param vendor
	 * @param product
	 * @param timeRange
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static List<Document> countTotalCvesForCpesAggregationStages(
			Cpe cpe,
			Optional<TimeRange> timeRange) throws UnsupportedEncodingException {
	
		Document aggregationMatch = aggregationMatch(cpe.getCpe_2_2(), timeRange);
		Document aggregationProject = Document.parse(AGGREGATION_COUNT);
		
		return Arrays.asList(aggregationMatch,
				aggregationProject);		
	}

	/**
	 * Turn array of fields into:
	 * 
	 * "field1": 1,
	 * "field2": 1,
	 * "field3": 1
	 * 
	 * @param fields
	 * @return
	 */
	private static String joinFields(String[] fields) {
		return Arrays.stream(fields)
			.map(f -> String.format("\"%s\": 1", f))
			.collect(Collectors.joining(","));
	}

}
