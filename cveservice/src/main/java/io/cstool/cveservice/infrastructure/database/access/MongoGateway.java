package io.cstool.cveservice.infrastructure.database.access;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.*;
import io.cstool.cveservice.infrastructure.database.config.MongoConfiguration;
import io.cstool.cveservice.infrastructure.database.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

/**
 * Generic mongodb DAO for querying DB- or collection-metadata.
 *
 * @author akoderman
 */
@Component
@Slf4j
public class MongoGateway {

    JsonWriterSettings jsonSettings;

    private static final String CVES_MODIFIED_SINCE = "{\"Modified\": {$gte: new ISODate(\"%s\")} }";

    private static final String AGGREGATION_EXPR_COUNT_CVES_MATCH = " { " + "        $match: { "
            + "            \"vulnerable_product_stems\": \"%s\"" + "        } " + "    }";

    private static final String AGGREGATION_EXPR_COUNT_CVES_MATCH_WITH_TIMERANGE = "{ " + "        $match: { "
            + "            \"vulnerable_product_stems\": \"%s\"," + "            \"Published\": {"
            + "                \"$gte\": new ISODate(\"%s\")," + "                \"$lte\": new ISODate(\"%s\")"
            + "            }" + "        } " + "}";

    private static final String AGGREGATION_EXPR_COUNT_CVES_PROJECT = "    {"
            + "        $project : {"
            + "            \"severity\" : {"
            + "                $cond : ["
            + "                    {$gt : [\"$cvss\", %.2f]}, "
            + "                    \"CRITICAL\", "
            + "                    {$cond : [ "
            + "                        {$gt : [\"$cvss\", %.2f]}, "
            + "                        \"HIGH\", "
            + "                        {$cond : [ "
            + "                            {$gt : [\"$cvss\", %.2f]}, "
            + "                            \"MEDIUM\", "
            + "                            \"LOW\" "
            + "                        ]}"
            + "                    ]}"
            + "                ]"
            + "            },"
            + "            \"exploitTrueCount\": { "
            + "                $cond : [\"$has_exploit\", 1, 0]"
            + "            }"
            + "        }"
            + "    }";

    private static final String AGGREGATION_EXPR_COUNT_CVES_GROUP = "    { " + "        $group: {"
            + "            \"_id\" : {" + "                \"severity\" : \"$severity\"," + "            },"
            + "            \"count\" : {\"$sum\" : 1} "
            + "            \"exploitCount\": {\"$sum\": \"$exploitTrueCount\"}" + "        }" + "    }";

    private static final String LATEST_NEWS_DATE_SEARCH = " {"
            + "        \"id\": \"%s\","
            + "        $or: ["
            + "            {"
            + "                \"latestNews\": {"
            + "                    \"$lte\": new ISODate(\"%s\")"
            + "                }"
            + "            },"
            + "            {"
            + "                \"latestNews\": null"
            + "            }"
            + "        ]"
            + "    }";
    private static final String LATEST_NEWS_DATE_UPDATE = " {" + "        $set: {"
            + "            \"latestNews\": new ISODate(\"%s\")" + "        }" + "    }" + "";

    private MongoConfiguration mongoConfiguration;

    Block<Document> printBlock = new Block<Document>() {
        @Override
        public void apply(final Document document) {
            System.out.println(document.toJson());
        }
    };

    private MongoCollection<Document> cpeCollection;
    private MongoCollection<Document> cveCollection;

    private CveNewsQuery newsQuery;

    @Autowired
    public MongoGateway(MongoConfiguration mongoConfig) {

        this.mongoConfiguration = mongoConfig;
        this.jsonSettings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();
        this.newsQuery = new CveNewsQuery();

        String protocol = Optional.ofNullable(mongoConfiguration.getProtocol()).orElse("mongodb");
        String user = Optional.ofNullable(mongoConfiguration.getUser()).orElse("user");
        String password = Optional.ofNullable(mongoConfiguration.getPassword()).orElse("password");
        String host = Optional.ofNullable(mongoConfiguration.getHost()).orElse("localhost");
        String database = Optional.ofNullable(mongoConfiguration.getDatabase()).orElse("test");

        MongoDatabase db;
        if ((mongoConfiguration.getUser() == null || mongoConfiguration.getUser().isEmpty())
                && mongoConfiguration.getHost() != null) {
            MongoClient mongoClient =
                    MongoClients.create("mongodb://" + mongoConfiguration.getHost() + ":" + mongoConfiguration.getPort());
            db = mongoClient.getDatabase("cvedb");
        } else {
            String uriString = String.format("%s://%s:%s@%s/%s?retryWrites=true&w=majority", protocol, user, password,
                    host, database);
            log.info("Mongodb URI String: " + uriString);
            MongoClient mongoClient = MongoClients.create(uriString);
            db = mongoClient.getDatabase(database);
        }

        cpeCollection = db.getCollection("cpe");
        cveCollection = db.getCollection("cves");
    }

    /**
     * Find CPEs starting with vendor or product name. Uses indices for vendor and
     * product with case-insensitive left-aligned regex match for speed.
     *
     * @param startsWith string to search for
     * @return JSON representation of found CPEs
     * @throws UnsupportedEncodingException
     */
    public String findCpeStartingWith(String startsWith) throws UnsupportedEncodingException {
        log.debug("Find CPE for search: " + startsWith);
        String decoded = URLDecoder.decode(startsWith, "UTF-8");
        log.debug("Urldecoded: " + startsWith);
        // String pattern = "^.*:.*:.*:.*" + decoded +".*:.*:.*";
        String pattern = "^" + startsWith;
        pattern = pattern.replace(" ", "_").toLowerCase();
        log.debug("Replaced pattern: " + pattern);

        FindIterable<Document> resultVend = cpeCollection.find(regex("vendor", pattern))
                .projection(include(Cpe.FIELDNAMES));

        FindIterable<Document> resultProd = cpeCollection.find(regex("product", pattern))
                .projection(include(Cpe.FIELDNAMES));

        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("[");
        final Boolean[] firstElement = new Boolean[]{true};
        Set<String> presentCpes = new HashSet<>();
        resultVend.forEach(doc -> {
            Cpe cpe = new Cpe(doc.getString("cpe_2_2"), doc.getString("title"));
            String commonCpe = cpe.getProductClassCpe();
            if (!presentCpes.contains(commonCpe)) {
                presentCpes.add(commonCpe);
                log.debug("Generic version of CPE: " + commonCpe);
                if (firstElement[0]) {
                    firstElement[0] = false;
                } else {
                    jsonBuilder.append(",");
                }
                doc.put("id", commonCpe);
                jsonBuilder.append(doc.toJson());
            }
        });
        resultProd.forEach(doc -> {
            Cpe cpe = new Cpe(doc.getString("cpe_2_2"), doc.getString("title"));
            String commonCpe = cpe.getProductClassCpe();
            if (!presentCpes.contains(commonCpe)) {
                presentCpes.add(commonCpe);
                log.debug("Generic version of CPE: " + commonCpe);
                if (firstElement[0]) {
                    firstElement[0] = false;
                } else {
                    jsonBuilder.append(",");
                }
                doc.put("id", commonCpe);
                jsonBuilder.append(doc.toJson());
            }
        });
        jsonBuilder.append("]");

        return jsonBuilder.toString();
    }

    public String findCvesForCpe(String cpe) throws UnsupportedEncodingException {
        return findCvesForCpe(cpe, new String[0]);
    }

    /**
     * Find CVEs for given vulnerable CPE.
     *
     * @param cpe    search string (in CPE format, i.e. "vendor:product"
     * @param fields return only the given fields (optional)
     * @return JSON representation of found CVEs
     * @throws UnsupportedEncodingException
     */
    public String findCvesForCpe(String cpe, String[] fields) throws UnsupportedEncodingException {
        cpe = URLDecoder.decode(cpe, "UTF-8");
        // special characters as replaced in cvedb:
        cpe = Cpe.replaceSpecialChars(cpe);
        log.debug("Search CVEs for CPE: " + cpe + " with fields: " + String.join(",", fields));

        // TODO sort by CVSS, create node with count of more  items

        FindIterable<Document> result;
        if (fields.length > 0) {
            result = cveCollection.find(regex("vulnerable_product", cpe))
                    .sort(orderBy(descending("cvss")))
                    .limit(100)
                    .projection(include(validateFields(fields, Cve.FIELDNAMES)));
        } else {
            result = cveCollection.find(regex("vulnerable_product", cpe));
        }

        return toJsonResult(result);
    }

    public boolean hasNews(String cpe, Optional<String> from, Optional<String> until) throws UnsupportedEncodingException {
        cpe = URLDecoder.decode(cpe, "UTF-8");
        cpe = Cpe.replaceSpecialChars(cpe);
        log.debug("Search News for CPE: " + cpe);

        Instant fromInst = from.map(this::toInstant).orElse(Instant.EPOCH);
        Instant toInst = until.map(this::toInstant).orElse(Instant.now());
        TimeRange cveRange = new TimeRange(fromInst, toInst);

        Document result = cveCollection.find(
                newsQuery.hasNews(cpe, cveRange)
        ).first();
        return result != null;
    }

    public boolean hasNews(String cpe, Optional<String> from, Optional<String> until, String recentNewsCutoff) throws UnsupportedEncodingException {
        cpe = URLDecoder.decode(cpe, StandardCharsets.UTF_8);
        cpe = Cpe.replaceSpecialChars(cpe);
        log.debug("Search News for CPE: " + cpe);

        Instant fromInst = from.map(this::toInstant).orElse(Instant.EPOCH);
        Instant toInst = until.map(this::toInstant).orElse(Instant.now());
        TimeRange cveRange = new TimeRange(fromInst, toInst);


        Document result = cveCollection.find(
                newsQuery.hasCurrentNews(cpe, cveRange, toInstant(recentNewsCutoff))
        ).first();
        return result != null;
    }

    private String toJsonResult(MongoIterable<Document> documents) {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("[");
        final Boolean[] firstElement = new Boolean[]{true};

        documents.forEach(doc -> {
            if (firstElement[0]) {
                firstElement[0] = false;
            } else {
                jsonBuilder.append(",");
            }
            jsonBuilder.append(doc.toJson(jsonSettings));
        });
        jsonBuilder.append("]");
        return jsonBuilder.toString();
    }

    private String toSingleJsonResult(MongoIterable<Document> documents) {
        if (documents.first() == null)
            return "{}";
        return documents.first().toJson(jsonSettings).toString();
    }

    private CveProductProjection toCveProductProjection(Document doc) throws IOException {
        log.debug("Read cve: " + doc.toJson(jsonSettings));
        var id = doc.getString("id");
        var products = doc.get("vulnerable_product_stems");
        return new CveProductProjection(id, (List<String>) products);
    }

    private List<CveProductProjection> toCveProductList(MongoIterable<Document> documents) {
        List<CveProductProjection> result = new ArrayList<>();
        documents.forEach(doc -> {
            try {
                result.add(toCveProductProjection(doc));
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new RuntimeException(e);
            }
        });
        return result;
    }

    /**
     * Count total number of CVEs by severity that match the given CPE.
     *
     * @return the number of CVEs matching the CPE as vulnerable_product grouped by
     * severity
     */
    public String summarizeCvesForCpe(String cpe, Optional<String> from, Optional<String> until)
            throws UnsupportedEncodingException {
        return toJsonResult(summarizeCvesForCpeAsBson(cpe, from.map(this::toInstant), until.map(this::toInstant)));
    }

    private Instant toInstant(String date) {
        return Instant.parse(date); // FIXME parse date exception
    }

//	public Map<String, CveSummary> summarizeCvesForCpeAsPOJO(String cpe) throws UnsupportedEncodingException {
//	    return toCveSummary(
//	        summarizeCvesForCpeAsBson(cpe)
//	    );
//	}

    private MongoIterable<Document> summarizeCvesForCpeAsBson(String cpe, Optional<Instant> from,
                                                              Optional<Instant> until) throws UnsupportedEncodingException {
        cpe = URLDecoder.decode(cpe, StandardCharsets.UTF_8);
        // special characters as replaced in cvedb:
        cpe = Cpe.replaceSpecialChars(cpe);
        log.debug("Count CVEs by severity for CPE: " + cpe);

        Document aggregationMatch;
        if (from.isEmpty() && until.isEmpty()) {
            aggregationMatch = Document.parse(String.format(AGGREGATION_EXPR_COUNT_CVES_MATCH, cpe));
        } else {
            aggregationMatch = Document.parse(String.format(AGGREGATION_EXPR_COUNT_CVES_MATCH_WITH_TIMERANGE, cpe,
                    from.get().toString(), until.get().toString()));
        }

        Document aggregationProject = Document.parse(String.format(Locale.US, AGGREGATION_EXPR_COUNT_CVES_PROJECT,
                Cve.CRITICAL_THRESHOLD, Cve.HIGH_THRESHOLD, Cve.MEDIUM_THRESHOLD));

        Document aggregationGroup = Document.parse(AGGREGATION_EXPR_COUNT_CVES_GROUP);

        return cveCollection.aggregate(Arrays.asList(aggregationMatch, aggregationProject, aggregationGroup));
    }

    /**
     * Count total number of CVEs that match the given CPE list.
     *
     * @return the number of CVEs matching the CPE as vulnerable_product
     * @throws UnsupportedEncodingException
     */
    public int countCvesForCpes(String cpeString, Optional<TimeRange> timeRange) throws UnsupportedEncodingException {
        Cpe cpe = new Cpe(cpeString, "");
        List<Document> stages = CveSearchAggregation.countTotalCvesForCpesAggregationStages(cpe, timeRange);
        log.debug("Aggregation pipeline for total count: " + stages.toString());
        Document result = cveCollection.aggregate(stages).first();
        return (result != null) ? result.getInteger(CveSearchAggregation.FIELD_COUNT) : 0;

//		Cpe cpe = new Cpe(cpeString, ""); 
//		
//		List<Document> stages = CveSearchAggregation.cvesForCpesAggregationStages(
//				cpe,
//				cpe.getVendor(),
//				cpe.getProduct(),
//				itemsPerPage,
//				requestedPage, 
//				validateFields(fields, Cve.FIELDNAMES), 
//				"cvss",
//				CveSearchAggregation.SORT_DESC,
//				published);
//		
//		log.info("Aggregation pipeline for search: " + stages.toString());
//		
//		return toJsonResult(
//				cveCollection.aggregate(stages)
//		);

        // xxx // TODO use new vendors/products fields
//		List<Bson> cpeFilters;
//	    Bson dateFilter;
//	      cpeFilters = Stream.of(cpes).map( cpe -> {
//    			return Filters.regex("vulnerable_product", "^"+replaceSpecialChars(escapeDotRequestBody(cpe)));
//    		}).collect(Collectors.toList());
//	    
//	    if (timeRange.isPresent()) {
//	        dateFilter = Filters.and( 
//	        				gte("Published", Date.from(timeRange.get().getFrom())),
//	        				lt("Published", Date.from(timeRange.get().getUntil()))
//    		);
//	        return cveCollection.count(
//        		Filters.and(
//        				dateFilter, 
//        				or(cpeFilters)
//        		)
//    		);
//	    }
//	    else {
//	    	return cveCollection.count(Filters.or(cpeFilters));
//	    }

    }

    /**
     * Find a paginated list of CVEs for a collection of given CPEs.
     *
     * @param itemsPerPage  How many items per pagination page should be returned.
     * @param requestedPage The number of the a the actual pge to be returned.
     * @param fields        Only return the CVE fields listed here.
     * @param published     timerange for the publication date
     * @return String with JSON representation of CVE reult list.
     * @throws UnsupportedEncodingException
     */
    public String findCvesForCpes(String cpeString, Integer itemsPerPage, Integer requestedPage, String[] fields,
                                  Optional<TimeRange> published) throws UnsupportedEncodingException {

        // debug output for mongodb query:
        // Logger.getLogger(Loggers.PREFIX).setLevel(Level.SEVERE);

        Cpe cpe = new Cpe(cpeString, "");

        List<Document> stages = CveSearchAggregation.cvesForCpesAggregationStages(cpe, itemsPerPage, requestedPage,
                validateFields(fields, Cve.FIELDNAMES), "cvss", CveSearchAggregation.SORT_DESC, published);

        log.debug("Aggregation pipeline for search: " + stages.toString());

        return toJsonResult(cveCollection.aggregate(stages));
    }

    /**
     * Make sure that all requested fields are valid by filtering out invalid
     * fields.
     *
     * @param requestedFields
     * @param validFields
     * @return
     */
    private String[] validateFields(String[] requestedFields, String[] validFields) {
        return Stream.of(requestedFields).filter(field -> Arrays.asList(validFields).contains(field))
                .toArray(String[]::new);
    }

    public Map<String, Object> getCollectionStats() {
        long countCpe = cpeCollection.estimatedDocumentCount();
        long countCve = cveCollection.estimatedDocumentCount();
        ObjectId objectID = (ObjectId) cveCollection.find().sort(descending("_id")).limit(1).first().get("_id");
        Date lastModifiedDate = objectID.getDate();
        return Stream
                .of(new Object[][]{{"cpeCount", countCpe}, {"cveCount", countCve},
                        {"lastModified", lastModifiedDate}})
                .collect(Collectors.toMap(data -> (String) data[0], data -> (Object) data[1]));
    }

    public String findCveById(String cveId) {
        FindIterable<Document> result;
        BasicDBObject query = new BasicDBObject("id", cveId);
        result = cveCollection.find(query);
        return toSingleJsonResult(result);
    }

    public Document findCveDocumentById(String cveId) {
        FindIterable<Document> result;
        BasicDBObject query = new BasicDBObject("id", cveId);
        result = cveCollection.find(query);
        return result.first();
    }

    public String findCveDbIdByCveId(String cveId) {
        FindIterable<Document> result;
        BasicDBObject query = new BasicDBObject("id", cveId);
        result = cveCollection.find(query);
        return result.first().get("_id").toString();
    }

    public void updateCveSetLatestNews(String cveId, Instant published) {
        log.info("Inserting latest news marker for CVE {} at {}", cveId, published);

        Document query = Document.parse(String.format(LATEST_NEWS_DATE_SEARCH, cveId, published.toString()));
        Document update = Document.parse(String.format(LATEST_NEWS_DATE_UPDATE, published.toString()));
        cveCollection.findOneAndUpdate(query, update);
    }

    public List<CveProductProjection> findNewCvesSince(Instant timestamp) {
        log.info("Searching for new CVEs since CVE id {}", timestamp);
        var query = Document.parse(String.format(CVES_MODIFIED_SINCE, timestamp.toString()));
        return toCveProductList(cveCollection.find(query));
    }

    private List<String> toIds(FindIterable<Document> documents) {
        return StreamSupport.stream(
                documents.map(document -> document.get("_id").toString()).spliterator(),
                false)
                .collect(Collectors.toList());
    }

    public String findNewestCveDbId() {
        log.info("Searching for newest CVE Id");
        var sortByIdDesc = new Document("_id", -1);
        ObjectId id = (ObjectId) cveCollection.find().sort(sortByIdDesc).limit(1)
                .first().get("_id");
        return id.toHexString();
    }


}
