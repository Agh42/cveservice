package io.cstool.cveservice.infrastructure.database.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "mongo")
@Getter
@Setter
@Configuration
public class MongoConfiguration {

	// properties are filled from application.properties or command line parameters:
		private String host;
		private String port;
		private String user;
		private String password;
		private String protocol;
		private String database;

}
