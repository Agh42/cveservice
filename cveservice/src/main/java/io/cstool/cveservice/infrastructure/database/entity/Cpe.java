package io.cstool.cveservice.infrastructure.database.entity;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Data
@AllArgsConstructor
/**
 * DTO for CPE entries.
 * * CPE id format is: cpe:cpeversion:type:vendor:product:
 * 						version:update:edition:lang:sw_edition:
 * 						target_sw:target_hw:other
 * @author akoderman
 *
 */
public class Cpe {

	@Size(max=1024)
	@Pattern(regexp = "cpe:2\\.[0-9]:[aho](?::(?:[a-zA-Z0-9!\"#$%&'()*+,\\\\\\-_.\\/;<=>?@\\[\\]^`{|}~]|\\\\:)+){10}$")
	private String cpe_2_2;

	@Size(max=1024)
	private String title;

	@JsonProperty("isActive")
	private boolean isActive;
	
	public static final String[] FIELDNAMES = {"cpe_2_2", "title"};

	public Cpe(String cpe, String title) {
		this(cpe, title, false);
	}
	
	public Cpe() {
		this("", "");
		// default constructor for serialization
	}

	/**
	 * Return generic form of this cpe, comprised of just vendor and product and stripped of version, update, edition etc.
	 * @return
	 */
	public String getProductClassCpe() {
	// fixme version number not alway removed. i.e. iphone_os
		String[] cpeParts = cpe_2_2.split(":");
		List<String> partsList = IntStream.range(0,  cpeParts.length)
			.mapToObj(i -> { return (i>4) ? "-" : cpeParts[i]; })
			.collect(Collectors.toList());
		while (partsList.size()<13) {
			partsList.add("-");
		}
		return String.join(":", partsList.toArray(new String[0]));
	}
	
	public String getVendor() {
		return cpe_2_2.split(":")[3];
	}
	
	public String getProduct() {
		return cpe_2_2.split(":")[4];
	}

	@JsonProperty
	public String getId() {
		return cpe_2_2;
	}

	/**
	 * Replace special cases as used in CVE-Search's database.
	 * 
	 * @param cpe
	 * @return
	 */
	public static String replaceSpecialChars(String cpe) {
		return cpe.replaceAll("\\(", "%28") // encode (
		.replaceAll("\\)", "%29") // encode )
		.replaceAll("!", "\\\\\\\\!")
		.replaceAll("\\^\\^", "/"); // encoded forward slash (%2F)is not allowed by spring in request so we use "^^" to replace it
	}
}
