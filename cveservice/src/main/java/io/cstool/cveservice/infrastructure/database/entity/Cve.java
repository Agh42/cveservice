package io.cstool.cveservice.infrastructure.database.entity;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;

/**
 * DTO for CVE entries.
 * @author akoderman
 *
 */
@Data
@RequiredArgsConstructor
public class Cve {

	public static final double MEDIUM_THRESHOLD = 3.99; // medium severity lower bound exclusive
	public static final double HIGH_THRESHOLD = 6.99; // high severity lower bound exclusive
	public static final double CRITICAL_THRESHOLD = 8.99; // critical severity lower bound exclusive

	@JsonAlias({"Modified"})
	private final String modified;

	@JsonAlias({"Published"})
	private final String published;

	private final String access;
	private final double cvss;
	private final String cvss_time;
	private final String cwe;
	private final String id;
	private final String impact;
	private final String last_modified;
	private final List references;
	private final String summary;
	private final List vulnerable_configuration;
	private final List vulnerable_product;

	public static final String[] FIELDNAMES = {
			"Modified", "Published", "access", "cvss",
			"cvss_time", "cwe", "id", "impact",
			"last-modified", "references", "summary",
			"vulnerable_configuration", "vulnerable_product",
			"vendors", "products", "vulnerable_product_stems", "vulnerable_configuration_stems",
			"cvssv3", "cvssv3-score", "references_tags", "has_exploit", "latestNews"};

	public Cve() {
		this("", "", "", 0D, "", "", "", "", "", null, "", null, null);
		// empty constructor for serialization
	}

}

