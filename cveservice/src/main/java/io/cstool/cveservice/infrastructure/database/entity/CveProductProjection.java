package io.cstool.cveservice.infrastructure.database.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CveProductProjection {
    private String cveId;
    private List<String> affectedProducts;
}
