package io.cstool.cveservice.infrastructure.database.entity;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.With;


// TODO move this to presenter dto
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CveSearchRequest {
	
		@NotEmpty
		@NotNull
		@Size(min=1, max=20)
		List<String> vulnerableCpes;
		
		@NotNull
		@NotEmpty
		@Size(min=1, max=50)
		List<String> fields;
		
		@NotNull
		@Min(value=10)
		@Max(value=100)
		Integer itemsPerPage;
		
		@NotNull
		@Positive
		Integer requestedPage;
		
		@Valid
		// may be null
		TimeRange published;

		public CveSearchRequest(List<String> vulnerableCpes,
				List<String> fields,
				Integer itemsPerPage, 
				 Integer requestedPage) {
			this.vulnerableCpes = vulnerableCpes;
			this.fields = fields;
			this.itemsPerPage = itemsPerPage;
			this.requestedPage = requestedPage;
		}
		
		
		
}

