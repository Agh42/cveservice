package io.cstool.cveservice.infrastructure.database.entity;


import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
/**
 * DTO for CVE summary entries.
 *
 */
public class CveSummary {
    private final String severity;
    private final int count;
}
