package io.cstool.cveservice.infrastructure.database.entity;

import java.time.Instant;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeRange {
	
	@PastOrPresent
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@NotNull
	Instant from; 
	
	@PastOrPresent
	@NotNull
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	Instant until; 
}
