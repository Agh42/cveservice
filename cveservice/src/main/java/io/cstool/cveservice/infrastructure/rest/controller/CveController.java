package io.cstool.cveservice.infrastructure.rest.controller;

import io.cstool.cveservice.adapters.controller.event.QueryEvent;
import io.cstool.cveservice.adapters.controller.event.SearchType;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.cstool.cveservice.infrastructure.database.entity.CveSearchRequest;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin(origins={"http://cstool.io", "http://attacksrfc.cstool.io",
    "https://attacksrfc.cstool.io", "http://localhost:3000"})
@RestController
@Validated
@RequestMapping("/api/v1/cve")
public class CveController {

	private MongoGateway mongoGateway;

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	public CveController(MongoGateway mongoRep) {
		mongoGateway = mongoRep;
	}

	@GetMapping("/{cveId}")
	public String getCveById(
			@PathVariable 
			@NotBlank 
			@Size(min=7, max=50) 
			@Pattern(regexp="CVE-\\d+-\\d+", flags=Pattern.Flag.CASE_INSENSITIVE) String cveId) {
		
			return mongoGateway.findCveById(cveId);
	}

	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}

}
