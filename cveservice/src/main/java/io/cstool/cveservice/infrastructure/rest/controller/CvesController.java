package io.cstool.cveservice.infrastructure.rest.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.cstool.cveservice.adapters.controller.event.QueryEvent;
import io.cstool.cveservice.adapters.controller.event.SearchType;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.cstool.cveservice.infrastructure.database.entity.Cpe;
import io.cstool.cveservice.infrastructure.database.entity.CveSearchRequest;
import lombok.extern.java.Log;

@CrossOrigin(origins = { "http://cstool.io", "http://attacksrfc.cstool.io", "https://attacksrfc.cstool.io",
		"http://localhost:3000" })
@RestController
@RequestMapping("/api/v1/cves")
@Log
public class CvesController {

	private MongoGateway mongoGateway;

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	public CvesController(MongoGateway mongoRep) {
		mongoGateway = mongoRep;
	}

	/**
	 * Returns a summary count of CVEs grouped by severity for the given CPE.
	 * 
	 * @param vulnerableCpe left aligned CPE URI format. NOTE: URL-encoded forward
	 *                      slashes are rejected. All forward slashes in the CPE
	 *                      have to be replaced by double carets instead: "^^"
	 * @return
	 */
	@RequestMapping(value = "/summary/vulnerable_product/{vulnerableCpe}", method = RequestMethod.GET)
	public String countCves(@PathVariable @NotBlank @NotNull String vulnerableCpe,
			@RequestParam(value = "publishedFrom") Optional<String> from,
			@RequestParam(value = "publishedUntil") Optional<String> until) {

		try {
			String summary = mongoGateway.summarizeCvesForCpe(vulnerableCpe, from, until);
			boolean hasNews = mongoGateway.hasNews(vulnerableCpe, from, until);
			boolean hasCurrentNews = mongoGateway.hasNews(vulnerableCpe, 
					from, 
					until, 
					Instant.now().minus(30, ChronoUnit.DAYS).toString());
			
			ObjectMapper mapper = new ObjectMapper();
			List summaryJson = mapper.readValue(summary, List.class);
			Map result = new HashMap<String, Object>();
			result.put("summary", summaryJson);
			result.put("hasNews", hasNews);
			result.put("hasNewsLast30Days", hasCurrentNews);
			
			return mapper.writeValueAsString(result);
		} catch (UnsupportedEncodingException e) {
			log.severe(e.getMessage());
			throw new IllegalArgumentException("error.searchString");
		} catch (JsonParseException e) {
			log.severe(e.getMessage());
			throw new IllegalArgumentException("error.searchString");
		} catch (JsonMappingException e) {
			log.severe(e.getMessage());
			throw new IllegalArgumentException("error.searchString");
		} catch (IOException e) {
			log.severe(e.getMessage());
			throw new IllegalArgumentException("error.searchString");
		}
	}

	/**
	 * Determine if there were news articles published for any CVEs of the given
	 * product in the given time frame.
	 * 
	 * @param vulnerableCpe      the product to search for
	 * @param from               search for CVEs published from this point in time
	 * @param until              search for CVEs published until this point in time
	 * @param newsPublishedSince only regard news published after this date
	 * @return
	 */
	@RequestMapping(value = "/summary/hasnews/{vulnerableCpe}", method = RequestMethod.GET)
	public String hasNews(@PathVariable @NotBlank @NotNull String vulnerableCpe,
			@RequestParam(value = "publishedFrom") Optional<String> from,
			@RequestParam(value = "publishedUntil") Optional<String> until,
			@RequestParam(value = "newsPublishedSince") Optional<String> newsPublishedSince) {
		boolean hasNews = false;
		try {
			if (newsPublishedSince.isEmpty()) {
				hasNews = mongoGateway.hasNews(vulnerableCpe, from, until);
			} else {
				hasNews = mongoGateway.hasNews(vulnerableCpe, from, until, newsPublishedSince.get());
			}
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("error.searchString");
		}
		return String.format("{\"hasNews\": \"%s\"}", hasNews);
	}

	/*
	 * // getter for base uri with limit and filter maybe
	 * 
	 * @GetMapping public Iterable findAll() { return cveRepository.findAll(); }
	 */

	@RequestMapping(value = "/vulnerable_product/{vulnerableCpe}", method = RequestMethod.GET)
	public String listCves(@PathVariable String vulnerableCpe,
			@RequestParam(value = "fields", required = false) String fieldsOrNull) {

		// TODO return exact cpe if id given

		Optional<String> optionalCpe = Optional.of(vulnerableCpe);
		if (!optionalCpe.isPresent())
			return "";

		Optional<String> optionalFields = Optional.ofNullable(fieldsOrNull);
		String[] fields = optionalFields.orElse("").split(",");

		//publisher.publishEvent(new QueryEvent("listCves", SearchType.UNLIMITED));

		try {
			return mongoGateway.findCvesForCpe(optionalCpe.get(), fields);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("error.searchString");
		}

	}

	/*
	 * @GetMapping("/inventory/{inventoryId}") public String listCvesForInventory(
	 * 
	 * @PathVariable String inventoryId,
	 * 
	 * @RequestParam(value = "fields", required = false) String fieldsOrNull ) {
	 * 
	 * // TODO return cves for inventory of cpes stored in the database by
	 * inventory-id}
	 */

	@PostMapping("/search")
	@ResponseStatus(HttpStatus.OK)
	public String searchCve(@RequestBody @Valid CveSearchRequest searchRequest) {
		try {
			// publisher.publishEvent(new QueryEvent("searchCve", SearchType.PAGINATED));

			if (searchRequest.getVulnerableCpes().size() < 1) {
				return "{\"resultCount\": 0," + "\"result\": {}" + "}";
			}
			int count = mongoGateway.countCvesForCpes(searchRequest.getVulnerableCpes().get(0),
					Optional.ofNullable(searchRequest.getPublished()));

			String searchResult = mongoGateway.findCvesForCpes(
					Cpe.replaceSpecialChars(searchRequest.getVulnerableCpes().get(0)), searchRequest.getItemsPerPage(),
					searchRequest.getRequestedPage(), searchRequest.getFields().toArray(new String[0]),
					Optional.ofNullable(searchRequest.getPublished()));
			return "{\"resultCount\": " + count + ", " + "\"result\": " + searchResult + "}";
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("error.searchString");
		}
	}

	/**
	 * Return summarized cve data as nodes and relations.
	 */
	/*
	 * // TODO return graph for a persisted inventory of cpes by id instead (get
	 * request)
	 * 
	 * @PostMapping("/graph")
	 * 
	 * @ResponseStatus(HttpStatus.OK) public String
	 * searchGraphView(@RequestBody @Valid CveSearchRequest searchRequest) { return
	 * ""; }
	 */

	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}

}
