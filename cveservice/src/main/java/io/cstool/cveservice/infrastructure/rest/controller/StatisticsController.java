package io.cstool.cveservice.infrastructure.rest.controller;

import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin(origins={"http://cstool.io", "http://attacksrfc.cstool.io",
    "https://attacksrfc.cstool.io", "http://localhost:3000"})
@RestController
@RequestMapping("/api/v1/stats")
public class StatisticsController {
	
	private MongoGateway mongoGateway;

	@Autowired
	public StatisticsController(MongoGateway mongoRep) {
		mongoGateway = mongoRep;
	}

	@GetMapping
	public Map<String, Object> stats() {
		return mongoGateway.getCollectionStats();
	}
	
    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, 
    		HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

}
