package io.cstool.cveservice.newsservice.entity;

import java.util.Date;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Valid
@Document
public class Article {

	public static enum SourceType {
		news,
		social,
		advisory,
		cert,
		github,
		exploitdb
	}
	
	@Id
	private String id;

	@NonNull
	@Size(min=0, max=50, message="Too many CVEs")
	@Indexed
	private Set<String> cvesMentioned;
	
	@NonNull
	@NotBlank(message = "Missing name")
	private String name;
	
	@NonNull
	@PastOrPresent(message = "Invalid publication date")
	@Indexed
	private Date datePublished;
	
	@NonNull
	@PastOrPresent(message = "Invalid retrieval date")
	@Indexed
	private Date dateRetrieved; 
	
	@NonNull
	@NotBlank(message = "Missing description")
	private String description;
	
	@NonNull
	@NotBlank(message = "Missing provider")
	@Size(min=2, max=3000, message = "Invalid provider length")
	private String provider;
	
	@NonNull
	@NotNull(message = "Missing URL")
	@NotBlank(message = "Missing URL")
	@Size(min=9, max=3000, message = "Invalid URL length")
	@Indexed(dropDups = true, unique = true)
	private String url;
	
	@NonNull
	@Indexed
	private SourceType sourceType;
	
	@NonNull
	@NotNull(message = "Missing lang")
	@NotBlank(message = "Misisng lang")
	@Size(min=2, max=255, message = "Invalid lang size")
	@Indexed
	private String lang;
	
	@NonNull
	@NotNull(message = "Missing region")
	@NotBlank(message = "Missing region")
	@Size(min=2, max=255, message = "Invalid region size")
	private String region;
	
}
