package io.cstool.cveservice.newsservice.entity;

import io.cstool.cveservice.notification.Template;
import io.cstool.cveservice.notification.entity.Content;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.*;

@Data
public class ArticleContent implements Content {

    public static final String TEMPLATE = "mail_hot_topics";

    public ArticleContent(Locale locale,
                          String recipientMail,
                          Date lastNews,
                          List<Article> articles) {
        this.locale = locale;
        model.put("recipientMail", recipientMail);
        model.put("lastNews", lastNews);
        model.put("articles", articles);
    }

    private Map<String, Object> model = new HashMap<>();

    private Locale locale;

    @Override
    public String getTemplateName() {
        return TEMPLATE;
    }
}
