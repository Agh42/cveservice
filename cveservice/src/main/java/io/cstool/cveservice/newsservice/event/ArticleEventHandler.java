package io.cstool.cveservice.newsservice.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.data.rest.core.event.AbstractRepositoryEventListener;

import io.cstool.cveservice.adapters.controller.event.QueryEvent;
import io.cstool.cveservice.adapters.controller.event.SearchType;
import io.cstool.cveservice.newsservice.entity.Article;

/**
 * React to repository events whenever a news article is stored.
 * Fires events to notify other services of the new news entry.
 * Uses internal spring application events.
 * For a distributed design this could be switched to RabbitMQ.
 *
 */
@RepositoryEventHandler
public class ArticleEventHandler  {

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@HandleAfterCreate
	protected void handleAfterCreate(Article article) {
		publisher.publishEvent(new NewsServiceEvent(
				NewsServiceEvent.NewsEventType.CREATE,
				article.getId(),
				article.getDatePublished(),
				article.getCvesMentioned()
		));
	}
	
	@HandleAfterSave
	protected void handleAfterSave(Article article) {
		publisher.publishEvent(new NewsServiceEvent(
				NewsServiceEvent.NewsEventType.SAVE,
				article.getId(),
				article.getDatePublished(),
				article.getCvesMentioned()
		));
	}
	
}
