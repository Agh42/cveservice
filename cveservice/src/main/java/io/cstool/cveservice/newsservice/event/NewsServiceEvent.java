package io.cstool.cveservice.newsservice.event;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Event class to publish the fact that a new article was discovered to interested parties.
 * <p>
 * The event just carries the immutable information that an article was found and stored.
 * <p>
 * Details of the article should be queried using the id against the synchronous REST API, since it could have been
 * changed or removed in the time it took the event to reach its listener. The order in which events are received could
 * also be different from the order in which they were created.
 */
@Data
@AllArgsConstructor
public class NewsServiceEvent implements DomainEvent {

    public static final String KEY = "cveservice.news_article_added";
    public static final String ARTICLE_ID = "articleId";

    public enum NewsEventType {
        CREATE, SAVE
    }

    private NewsEventType eventType;
    private String articleId;
    private Date published;
    private Set<String> cvesMentioned;

    @Override
    public String getId() {
        return articleId;
    }

    @Override
    public Set<String> getAffectedCves() {
        return cvesMentioned;
    }

    @Override
    public String getRoutingKey() {
        return KEY;
    }

    @Override
    public Map<String, Serializable> getData() {
        return Map.of("articleId", articleId);
    }
}
