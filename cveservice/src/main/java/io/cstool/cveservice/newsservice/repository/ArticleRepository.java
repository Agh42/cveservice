package io.cstool.cveservice.newsservice.repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import io.cstool.cveservice.newsservice.entity.Article;

//@RepositoryRestResource(collectionResourceRel = "articles", path = "articles")
@CrossOrigin(origins={"http://cstool.io", "http://attacksrfc.cstool.io",
  "https://attacksrfc.cstool.io", "http://localhost:3000"})
public interface ArticleRepository extends MongoRepository<Article, String> {
	
	List<Article> findDistinctByCvesMentionedInOrderByDatePublishedDesc(@Param("cve") String cve);
	
	Article findByUrl(@Param("url") String url);

	List<Article> findByDatePublishedAfter(@Param("date") Date date);
}
