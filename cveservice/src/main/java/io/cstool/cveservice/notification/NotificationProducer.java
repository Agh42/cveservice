package io.cstool.cveservice.notification;

import io.cstool.cveservice.account.entity.Account;
import io.cstool.cveservice.account.entity.Inventory;
import io.cstool.cveservice.account.entity.UserInfo;
import io.cstool.cveservice.account.repository.AccountRepository;
import io.cstool.cveservice.adapters.controller.event.CveSummaryEvent;
import io.cstool.cveservice.adapters.notification.CveNotificationEntry;
import io.cstool.cveservice.adapters.notification.CveUpdatesContent;
import io.cstool.cveservice.eventstore.entity.StoredEvent;
import io.cstool.cveservice.eventstore.service.EventStore;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.cstool.cveservice.infrastructure.database.entity.Cpe;
import io.cstool.cveservice.newsservice.entity.Article;
import io.cstool.cveservice.newsservice.entity.ArticleContent;
import io.cstool.cveservice.newsservice.repository.ArticleRepository;
import io.cstool.cveservice.notification.entity.Notification;
import io.cstool.cveservice.notification.entity.Recipient;
import io.cstool.cveservice.notification.event.NotificationsSentEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;

/**
 * A timer bean that collates events from the event store to produce digest notifications to users about their
 * inventories and recent news.
 * <p>
 * The time at which the notifications are produced and the notification service being used are determined by the
 * settings in the application's properties file or using environment variables.
 */
@Component
public class NotificationProducer {

    public static final String SUBJECT_NOT_CONFIGURED = "<subject not configured>";
    @Autowired
    EventStore eventStore;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    NotificationSender sender;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    MongoGateway mongoGateway;

    @Value("${cveservice.notifications.hottopics.header}")
    public String newsNotificationHeader;

    @Value("${cveservice.notifications.inventories.header}")
    public String inventoriesNotificationHeader;

    @Scheduled(cron = "${cveservice.notifications.cron}")
    public void produceNotifications() {
        StoredEvent lastEvent = getLastPublishedNotificationEvent();

        // get news since last notification was sent out:
        var theNews = articleRepository.findByDatePublishedAfter(lastEvent.getOcurredOn());

        // get cves (with affected products) since last notification was sent:
        Map<String, List<String>> cves = getNewCvesWithProducts(lastEvent.getOcurredOn());

        // get all accounts:
        List<Account> accounts = accountRepository.findAll();

        // send notifications on news:
        accounts.stream()
                .filter(account -> account.getPreferences().isNotificationsHotTopics())
                .filter(account -> ofNullable(account.getUserInfo())
                        .orElse(UserInfo.builder().build())
                        .isEmailVerified())
                .map(account -> newArticleNotificationFor(account, theNews, lastEvent.getOcurredOn()))
                .forEach(notification -> sender.send(notification));

        // send notifications on inventories:
        accounts.stream()
                .filter(account -> account.getPreferences().isNotificationsInventories())
                .filter(account -> ofNullable(account.getUserInfo())
                        .orElse(UserInfo.builder().build())
                        .isEmailVerified())
                .map(account -> newCVEsNotificationFor(account, cves))
                // send only if there are affected inventories:
                .filter(
                        notification -> ((CveUpdatesContent) notification.getContent()).hasAffectedInventories()
                )
                .forEach(notification -> sender.send(notification));

        eventStore.append(new NotificationsSentEvent());
    }

    public Notification newCVEsNotificationFor(Account account, Map<String, List<String>> cves) {
        var builder = Notification.builder();
        builder.header(requireNonNullElse(inventoriesNotificationHeader, SUBJECT_NOT_CONFIGURED));
        builder.recipient(Recipient.of(account));

        var inventoriesNotifyEnabled = account.getInventories().stream()
                .filter(Inventory::isNotify)
                .collect(Collectors.toSet());

        var notificationEntries = cves.entrySet().stream()
                .map(
                        entry -> toNotificationEntry(entry.getKey(), entry.getValue(), inventoriesNotifyEnabled)
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(cveNotificationEntry -> cveNotificationEntry.getAffectedInventories().size() > 0)
                .collect(Collectors.toUnmodifiableList());

        builder.content(new CveUpdatesContent(Locale.US, account.getEmail(), notificationEntries));
        builder.templateName(CveUpdatesContent.TEMPLATE);
        return builder.build();
    }

    private Optional<CveNotificationEntry> toNotificationEntry(String cveId, List<String> affectedProducts, Set<Inventory> inventories) {
        var cve = mongoGateway.findCveDocumentById(cveId);
        if (cve == null)
            return Optional.empty();
        var builder = CveNotificationEntry.builder();

        builder.vulnerability(cveId);
        builder.affectedProducts(affectedProducts);
        builder.cvssv2(
                (cve.get("cvss") instanceof Double)
                        ? String.valueOf(cve.get("cvss"))
                        : "-"
        );
        builder.cvssv3(
                (cve.get("cvssv3_score") instanceof Double)
                        ? String.valueOf(cve.get("cvssv3_score"))
                        : "-"
        );

        builder.affectedInventories(
                inventories.stream().filter(
                        inventory -> inventoryIsAffected(inventory.getProducts(), affectedProducts)
                ).map(Inventory::getName)
                        .collect(Collectors.toList())
        );

        builder.summary(cve.getString("summary"));
        builder.hasExploit(cve.getBoolean("has_exploit"));

        return Optional.of(builder.build());
    }

    private boolean inventoryIsAffected(List<Cpe> products, List<String> affectedProducts) {
        for (String p : affectedProducts) {
            var anyMatch = products.stream()
                    .anyMatch(cpe -> cpe.getProductClassCpe().matches(".*" + p + ".*"));
            if (anyMatch)
                return true;
        }
        return false;
    }

    public Notification newArticleNotificationFor(Account account, List<Article> theNews,
                                                  Date occurredOn) {
        var builder = Notification.builder();
        builder.header(requireNonNullElse(newsNotificationHeader, SUBJECT_NOT_CONFIGURED));
        builder.content(new ArticleContent(Locale.US,
                account.getEmail(), occurredOn, theNews));
        builder.recipient(Recipient.of(account));
        builder.templateName(ArticleContent.TEMPLATE);
        return builder.build();
    }

    private StoredEvent getLastPublishedNotificationEvent() {
        return eventStore.findNewestEvent(NotificationsSentEvent.KEY)
                .orElse(publishFirstEvent());
    }

    private StoredEvent publishFirstEvent() {
        return eventStore.append(new NotificationsSentEvent());
    }

    public Map<String, List<String>> getNewCvesWithProducts(Date published) {
        var result = new HashMap<String, List<String>>();
        eventStore.findEventsNewerThan(CveSummaryEvent.KEY, published)
                .stream()
                .map(StoredEvent::getOriginalEvent)
                .map(dbObject -> (Map) dbObject.get(CveSummaryEvent.AFFECTED_PRODUCTS))
                .forEach(m -> addEntries(result, m));
        return result;
    }

    private void addEntries(Map<String, List<String>> existingMap,
                            Map<String, List<String>> newEntries) {
        newEntries.keySet().forEach(key -> {
            List<String> products = existingMap.getOrDefault(key, new ArrayList<>());
            products.addAll(newEntries.get(key));
            existingMap.put(key, products);
        });
    }
}
