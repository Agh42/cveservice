package io.cstool.cveservice.notification;

import io.cstool.cveservice.notification.entity.Content;
import io.cstool.cveservice.notification.entity.Notification;
import io.cstool.cveservice.notification.entity.Recipient;

import javax.mail.MessagingException;

/**
 * Fills in a template with provided data and sends it to the recipient.
 */
public interface NotificationSender {
    void send(Notification notification);
}
