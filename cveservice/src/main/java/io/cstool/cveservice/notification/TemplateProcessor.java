package io.cstool.cveservice.notification;

import io.cstool.cveservice.notification.entity.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
public class TemplateProcessor {

    @Autowired
    private SpringTemplateEngine templateEngine;

    public String processTemplate(Notification notification) {
        Context context = new Context();
        context.setVariables(notification.getContent().getModel());
        return templateEngine.process(notification.getTemplateName(), context);
    }
}
