package io.cstool.cveservice.notification.aws;

import io.cstool.cveservice.notification.NotificationSender;
import io.cstool.cveservice.notification.entity.Notification;
import io.cstool.cveservice.notification.TemplateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class SESNotificationSender implements NotificationSender {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${cveservice.notifications.from}")
    private String fromAddress;

    @Autowired
    TemplateProcessor templateProcessor;

    @Override
    @Async
    public void send(Notification notification) {
        try {
            javaMailSender.send(toMimeMessage(notification));
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private MimeMessage toMimeMessage(Notification notification) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper =
                new MimeMessageHelper(
                        message,
                        MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());

        String html = templateProcessor.processTemplate(notification);

        helper.setTo(notification.getRecipient().getEmailAddress());
        helper.setText(html, true);
        helper.setSubject(notification.getHeader());
        helper.setFrom(fromAddress);
        return message;
    }


}
