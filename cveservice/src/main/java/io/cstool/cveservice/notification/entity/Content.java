package io.cstool.cveservice.notification.entity;

import lombok.Data;
import lombok.Value;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

public interface Content {
    Map<String, Object> getModel();

    Locale getLocale();

    String getTemplateName();

}
