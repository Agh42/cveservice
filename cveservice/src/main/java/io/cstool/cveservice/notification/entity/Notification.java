package io.cstool.cveservice.notification.entity;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@Value
@With
@Builder
public class Notification {

    @NotNull @NonNull String header;

    @NotNull @NonNull String templateName;

    @NotNull @NonNull Content content;

    @NotNull @NonNull Recipient recipient;


}
