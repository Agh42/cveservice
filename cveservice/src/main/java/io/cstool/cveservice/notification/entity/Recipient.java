package io.cstool.cveservice.notification.entity;

import io.cstool.cveservice.account.entity.Account;
import lombok.Value;

@Value
public class Recipient {
    String id;
    String emailAddress;

    public static Recipient of(Account account) {
        return new Recipient(account.getId(), account.getEmail());
    }
}
