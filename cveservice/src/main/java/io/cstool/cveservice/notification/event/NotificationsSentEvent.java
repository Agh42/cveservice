package io.cstool.cveservice.notification.event;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import lombok.Data;
import lombok.NonNull;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.Instant;
import java.util.*;

@Data
public class NotificationsSentEvent implements DomainEvent {

    public static final String KEY = "cveservice.notifications_sent";

    private String id = UUID.randomUUID().toString();

    @NonNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date published;

    public NotificationsSentEvent() {
        this.published = new Date();
    }

    @Override
    public Set<String> getAffectedCves() {
        return Collections.emptySet();
    }

    @Override
    public String getRoutingKey() {
        return KEY;
    }

    @Override
    public Map<String, Serializable> getData() {
        return Collections.emptyMap();
    }
}
