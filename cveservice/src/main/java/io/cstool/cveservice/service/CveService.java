package io.cstool.cveservice.service;

import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Set;

@Service
public class CveService {

    @Autowired
    private MongoGateway mongoGateway;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateLatestNews(Set<String> cvesMentioned, Instant published) {
        cvesMentioned.forEach(cve -> {
            mongoGateway.updateCveSetLatestNews(cve, published);
        });
    }
}
