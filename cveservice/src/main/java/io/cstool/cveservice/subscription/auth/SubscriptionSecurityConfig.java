package io.cstool.cveservice.subscription.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Configuration
@EnableWebSecurity
@Order(3)
public class SubscriptionSecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String ANT_MATCHER = "/**/subscriptions";

	// API key auth from other services:
	@Value("${subscriptionservice.apikey.header}")
	private String principalRequestHeader;

	@Value("${subscriptionservice.apikey.value}")
	private String principalRequestValue;


	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		
		ApiKeyFilter filter = new ApiKeyFilter(principalRequestHeader);
		filter.setAuthenticationManager(new AuthenticationManager() {

			@Override
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				String principal = (String) authentication.getPrincipal();
				if (!principalRequestValue.equals(principal)) {
					throw new BadCredentialsException("Invalid API Key");
				}
				authentication.setAuthenticated(true);
				return authentication;
			}
		});
		
		httpSecurity
			// allow write access to subscriptions only with api key:
			.requestMatchers()
				.antMatchers(HttpMethod.POST, ANT_MATCHER)
				.antMatchers(HttpMethod.PUT, ANT_MATCHER)
				.antMatchers(HttpMethod.DELETE, ANT_MATCHER)
				.antMatchers(HttpMethod.PATCH, ANT_MATCHER)
				.and()
				.csrf()
					.disable()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilter(filter)
				.authorizeRequests()
					.anyRequest()
					.authenticated();
	}
	
	
}
