package io.cstool.cveservice.subscription.auth;

import io.cstool.cveservice.newsservice.auth.ApiKeyFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Configuration
@EnableWebSecurity
@Order(4)
public class VoucherSecurityConfig extends WebSecurityConfigurerAdapter {

	// API key auth:
	@Value("${newsservice.apikey.header}")
	private String principalRequestHeader;

	@Value("${newsservice.apikey.value}")
	private String principalRequestValue;


	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		
		io.cstool.cveservice.newsservice.auth.ApiKeyFilter filter = new ApiKeyFilter(principalRequestHeader);
		filter.setAuthenticationManager(new AuthenticationManager() {

			@Override
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				String principal = (String) authentication.getPrincipal();
				if (!principalRequestValue.equals(principal)) {
					throw new BadCredentialsException("Invalid API Key");
				}
				authentication.setAuthenticated(true);
				return authentication;
			}
		});
		
		httpSecurity
			// allow read & write access to vouchers only with api key:
			.requestMatchers()
				.antMatchers(HttpMethod.POST, "/**/vouchers/**")
				.antMatchers(HttpMethod.GET, "/**/vouchers/**")
				.antMatchers(HttpMethod.PUT, "/**/vouchers/**")
				.antMatchers(HttpMethod.DELETE, "/**/vouchers/**")
				.antMatchers(HttpMethod.PATCH, "/**/vouchers/**")
				.and()
				.csrf()
					.disable()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilter(filter)
				.authorizeRequests()
					.anyRequest()
					.authenticated();
	}
}
