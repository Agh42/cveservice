package io.cstool.cveservice.subscription.controller;


import io.cstool.cveservice.account.service.AccountService;
import io.cstool.cveservice.subscription.entity.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = { "http://cstool.io", "http://attacksrfc.cstool.io", "https://attacksrfc.cstool.io",
        "http://localhost:3000" })
@RestController
@RequestMapping("/api/v1/subscriptions")
public class SubscriptionsController {

    private final AccountService accountService;

    @Autowired
    SubscriptionsController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PutMapping(value="/{subscriptionId}")
    public void saveSubscription(String subscriptionId, @RequestBody @Valid Subscription subscription) {
        accountService.updateSubscription(subscriptionId, subscription);
    }

    @PostMapping
    public void createSubscription(@RequestBody @Valid Subscription subscription) {
        accountService.createSubscription(subscription);
    }

}
