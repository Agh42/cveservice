package io.cstool.cveservice.subscription.entity;

import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.cstool.cveservice.account.entity.Inventory;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.Nullable;

import java.time.Instant;
import java.util.Optional;

@Value
@With
@Builder
@Valid
@Document
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonDeserialize(builder = Subscription.SubscriptionBuilder.class)
public class Subscription {

	@Id
	@EqualsAndHashCode.Include
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private final String id;

	@NotNull
	@NotEmpty
	@Email
	private final String email;
	
	@NotNull
	@NonNull
	private final String name;
	
	@NotNull
	@PositiveOrZero
	@Max(value = 1000)
	private final long maxInventories;
	
	@NotNull
	@PositiveOrZero
	@Max(value = 500)
	private final long maxProductsPerInventory;

	@NotNull
	@FutureOrPresent
	private final Instant validUntil;

	@NotNull
	@PastOrPresent
	private final Instant createdOn;

	@Nullable
	private final String usedVoucherCode;

	public Subscription cancel() {
		return this.withValidUntil(Instant.now());
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class SubscriptionBuilder {
		// required for Jackson
	}

	public Optional<String> getUsedVoucherCode() {
		return Optional.ofNullable(usedVoucherCode);
	}
}
