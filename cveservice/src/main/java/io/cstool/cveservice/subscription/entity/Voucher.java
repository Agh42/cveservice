package io.cstool.cveservice.subscription.entity;

import com.mongodb.DBObject;
import io.cstool.cveservice.account.AccountException;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * A voucher that can be used to create a subscription for an account.
 */
@Data
@Valid
@Document
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Voucher {

    @Id
    private String id;

    /**
     * The voucher code entered by the user
     */
    private String code;

    /**
     * How often may this voucher be used?
     */
    private int maxUsed;

    /**
     * How often was this voucher used already?
     */
    private int numUsed;

    /**
     * Until when may this voucher be redeemed?
     */
    private Instant validUntil;

    /**
     * Only accounts with email matching this pattern may redeem the voucher. (Regex pattern.)
     */
    @NotNull
    @NotEmpty
    private String validEmailPattern;

    /**
     * Name for the subscription to be created.
     */
    @NotNull
    @NonNull
    private String subsName;

    /**
     * For how many day is the created subscription valid?
     */
    private int subsValidDays;

    /**
     * Max inventory setting for the subscription.
     */
    @NotNull
    @PositiveOrZero
    @Max(value = 1000)
    private long subsMaxInventories;

    /**
     * Max products-pre-inventory setting for the subscription.
     */
    @NotNull
    @PositiveOrZero
    @Max(value = 500)
    private long subsMaxProductsPerInventory;


    /**
     * Creates a new subscription from this voucher. This will increase the usage count by one.
     *
     * @param email the email for which the subscription is created.
     * @return the created subscription
     */
    @Transactional
    public Subscription redeem(String email) {
        if (!email.matches(validEmailPattern))
            throw new IllegalArgumentException("Voucher code is not valid for this email address.");
        if (Instant.now().isAfter(validUntil))
            throw new AccountException("This voucher code has expired.");
        if (numUsed >= maxUsed)
            throw new AccountException("This voucher code has been used too many times.");

        numUsed++;
        return Subscription.builder()
                .name(subsName)
                .maxInventories(subsMaxInventories)
                .maxProductsPerInventory(subsMaxProductsPerInventory)
                .validUntil(Instant.now().plus(subsValidDays, ChronoUnit.DAYS))
                .createdOn(Instant.now())
                .id(UUID.randomUUID().toString())
                .email(email)
                .usedVoucherCode((code))
                .build();
    }
}
