package io.cstool.cveservice.subscription.event;

import io.cstool.cveservice.subscription.entity.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import io.cstool.cveservice.newsservice.entity.Article;

/**
 * React to repository events whenever a news subscription is stored.
 * Fires events to notify other services of the new news entry.
 * Uses internal spring application events for now. 
 * For a distributed design this could be switched to RabbitMQ.
 *
 */
@RepositoryEventHandler
public class SubscriptionEventHandler {

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@HandleAfterCreate
	protected void handleAfterCreate(Subscription subscription) {
		publisher.publishEvent(new SubscriptionServiceEvent(
				SubscriptionServiceEvent.EventType.CREATE,
				subscription.getId()
		));
	}
	
	@HandleAfterSave
	protected void handleAfterSave(Subscription subscription) {
		publisher.publishEvent(new SubscriptionServiceEvent(
				SubscriptionServiceEvent.EventType.SAVE,
				subscription.getId()
		));
	}
	
}
