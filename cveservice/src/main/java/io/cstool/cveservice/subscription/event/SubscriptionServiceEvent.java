package io.cstool.cveservice.subscription.event;

import java.util.Date;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
 * Event class to publish the fact that a new article
 * was discovered to interested parties.
 * The event just carries the immutable information that an article was
 * found and stored.
 * Details of the article should be queried using the id against the synchronous REST API, 
 * since it could have been changed or removed in
 * the time it took the event to reach its listener. The order in which events
 * are received could also be different from the order in which they were created.
 * 
 */
@Data
@AllArgsConstructor
public class SubscriptionServiceEvent {

	public enum EventType {
		CREATE, SAVE
	}

	private EventType eventType;
	private String id;

}
