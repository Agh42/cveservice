package io.cstool.cveservice.subscription.repository;

import io.cstool.cveservice.subscription.entity.Voucher;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface VoucherRepository extends MongoRepository<Voucher, String> {

    Optional<Voucher> findFirstByCodeIgnoreCase(String code);

}
