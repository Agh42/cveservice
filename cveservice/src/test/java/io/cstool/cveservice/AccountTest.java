package io.cstool.cveservice;

import io.cstool.cveservice.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"dev", "test"})
@Slf4j
public class AccountTest {

    @Autowired
    AccountService accountService;

    @Test
    public void removeAccountAffiliations() throws InterruptedException {

        // given: a tenant with a boss account and one additional affiliated account
        var boss1 = accountService.createBossAccount(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()+"@test.example", null);
        var boss2 = accountService.createBossAccount(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()+"@test.example", null);

        var invite = boss1.invite(boss2); // (real invite is received via REST request body)
        invite = accountService.inviteUser(boss1.getExternalUserId(), invite);
        accountService.acceptInvite(boss2.getExternalUserId(), invite.getEntityId());

        boss2 = accountService.findByExternalUserId(boss2.getExternalUserId());
        assertEquals(1, boss2.getAffiliations().size());

        // when: the boss account is removed
        accountService.delete(boss1.getExternalUserId());
        Thread.sleep(200); // give the async event handlers some time

        // then: the affiliation is removed from user2
        boss2 = accountService.findByExternalUserId(boss2.getExternalUserId());
        assertEquals(0, boss2.getAffiliations().size());
    }
}
