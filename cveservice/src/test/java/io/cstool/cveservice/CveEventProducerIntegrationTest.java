package io.cstool.cveservice;

import io.cstool.cveservice.adapters.controller.event.CveEventProducer;
import io.cstool.cveservice.adapters.controller.event.CveSummaryEvent;
import io.cstool.cveservice.eventstore.service.EventStore;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;


//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"dev", "test"})
@Slf4j
public class CveEventProducerIntegrationTest {

    @Autowired
    EventStore eventStore;

    @Autowired
    MongoGateway mongoGateway;

    @Autowired
    CveEventProducer eventProducer;

    @Autowired
    EventCatcher eventCatcher;

    @Autowired
    private ApplicationEventPublisher publisher;


    @Test
    public void produceSummaryEvent() {
        //given: a collection of CVEs
        //TODO: create test dataset, currently expects a database initialized by the cve-search tool

        //when: the regularly scheduled method is called
        // eventProducer.produceEvents(); //simulate first event (just the latest CVE with no "new-cves" list

        // when: we simulate a previously stored event for an older CVE:
        //var cveDbId = mongoGateway.findCveDbIdByCveId("CVE-2020-9770");
//        publisher.publishEvent(new CveSummaryEvent(
//                Collections.emptySet(),
//                Date.from(Instant.parse("2020-04-01T00:00:00Z")),
//                cveDbId));

        //then:
        //assertTrue("An event was stored", eventStore.findNewestEvent(CveSummaryEvent.KEY).isPresent());
        //assertEquals("An event was published", 1, eventCatcher.getReceivedEvents().size());

        //when: the scheduled event producer is called:
        eventProducer.produceEvents();

        //then:
        assertTrue("An event was stored", eventStore.findNewestEvent(CveSummaryEvent.KEY).isPresent());
        //assertEquals("Another event was published", 2, eventCatcher.getReceivedEvents().size());
        assertTrue("The list of new CVEs was produced.",
                eventStore.findNewestEvent(CveSummaryEvent.KEY).get().getAffectedCves().size()>8);

        // and: the stored event contains CVEs and their affected products:
        var event = eventStore.findNewestEvent(CveSummaryEvent.KEY).get().getOriginalEvent();
        Document doc = (Document) event.get(CveSummaryEvent.AFFECTED_PRODUCTS);
        var cveProductsEntry = doc.entrySet().iterator().next();
        assertTrue("A CVE id is present.", cveProductsEntry.getKey().matches("^CVE-.*"));
        List<String> products = (List<String>) cveProductsEntry.getValue();
        assertTrue("The products for the CVE are present", products.size()>0);

    }

}