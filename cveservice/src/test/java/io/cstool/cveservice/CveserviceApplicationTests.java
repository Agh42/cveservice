package io.cstool.cveservice;

import io.cstool.cveservice.infrastructure.database.entity.CveSearchRequest;
import io.cstool.cveservice.infrastructure.database.entity.TimeRange;
import io.cstool.cveservice.infrastructure.rest.controller.CpeController;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("dev")
@Slf4j
public class CveserviceApplicationTests {

	 private static final String JSON_CONTENT_TYPE = "application/json;charset=UTF-8"; 

	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Autowired
	private CpeController cpeController;
	
	@Test
	public void contextLoads() {
		assertThat(cpeController).isNotNull();
	}
	
	@Test
	public void searchWithTimerange() {
		List<String> fields = List.of("id", "cvss", "vulnerable_product", "vulnerable_configuration");
		List<String> cpes = List.of("cpe:2.3:o:microsoft:windows_xp");
		TimeRange published = new TimeRange(Instant.parse("2010-01-01T00:00:00Z"), Instant.now());
		CveSearchRequest body = new CveSearchRequest(cpes, fields, 20, 1, published);
		
		HttpEntity<CveSearchRequest> request = new HttpEntity<CveSearchRequest>(body);
		log.info("Search request body: " + request.toString());
		ResponseEntity<String> response = this.restTemplate.postForEntity("/api/v1/cves/search",
				request, String.class);
		
		log.info("Search with range response: " + response.toString().substring(0, 80));
		assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	public void searchWithoutTimerange() {
		List<String> fields = List.of("id", "cvss", "vulnerable_product", "vulnerable_configuration");
		List<String> cpes = List.of("cpe:2.3:o:microsoft:windows_xp");
		CveSearchRequest body = new CveSearchRequest(cpes, fields, 20, 1);
		
		HttpEntity<CveSearchRequest> request = new HttpEntity<CveSearchRequest>(body);
		ResponseEntity<String> response = this.restTemplate.postForEntity("/api/v1/cves/search",
				request, String.class);
		
		log.info("Search response: " + response.toString().substring(0, 80));
		assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	public void summarizeWithoutTimerange() {
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("vulnerableCpe", "cpe:2.3:o:microsoft:windows_xp");
		
		ResponseEntity<String> response = this.restTemplate
				.getForEntity(
						"/api/v1/cves/summary/vulnerable_product/{vulnerableCpe}",
						String.class,
						requestParams
				);
		
		
		log.info("Summary response: " + response);
		assertEquals(200, response.getStatusCodeValue());
	}
	
	@Test
	public void summarizeWithTimerange() {
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("vulnerableCpe", "cpe:2.3:o:microsoft:windows_xp");
		requestParams.put("publishedFrom", "2010-10-01T00:00:00Z");
		requestParams.put("publishedUntil", "2020-01-01T00:00:00Z");
		
		ResponseEntity<String> response = this.restTemplate
				.getForEntity(
						"/api/v1/cves/summary/vulnerable_product/{vulnerableCpe}"
						+ "?publishedFrom={publishedFrom}"
						+ "&publishedUntil={publishedUntil}",
						String.class,
						requestParams
				);
		
		
		log.info("Summary with range response: " + response);
		assertEquals(200, response.getStatusCodeValue());
	}

}
