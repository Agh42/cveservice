package io.cstool.cveservice;

import io.cstool.cveservice.eventstore.entity.DomainEvent;
import lombok.Data;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
/**
 * Event listener to be used in tests.
 */
@Data
public class EventCatcher {

    private List<DomainEvent> receivedEvents = new LinkedList<>();

    @EventListener
    public void catchEvent(DomainEvent event) {
        receivedEvents.add(event);
    }
}
