package io.cstool.cveservice;

import io.cstool.cveservice.adapters.controller.event.CveSummaryEvent;
import io.cstool.cveservice.eventstore.service.EventStore;
import io.cstool.cveservice.infrastructure.database.access.MongoGateway;
import io.cstool.cveservice.newsservice.event.NewsServiceEvent;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Date;

import static org.springframework.test.util.AssertionErrors.assertTrue;


//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"dev", "test"})
@Slf4j
public class EventStoreIntegrationTest {

    @Autowired
    EventStore eventStore;

    @Autowired
    MongoGateway mongoGateway;

    @Test
    public void findLastStoredCveSummaryEvent() {
        //given: two stored events
        eventStore.append(
                new CveSummaryEvent(Collections.emptySet(),
                        new Date(),
                        "CVE-2020-1"));
        eventStore.append(
                new CveSummaryEvent(Collections.emptySet(),
                        new Date(),
                        "CVE-2020-2"));

        // when: the newest stored event is requested:
        var storedEvent = eventStore.findNewestEvent(CveSummaryEvent.KEY);

        //then:
        assertTrue("The stored event is retrieved.", storedEvent.isPresent());
        assertTrue("The type is correct.", storedEvent.get().getType().equals(CveSummaryEvent.KEY));
        assertTrue("The id is correct.",
                storedEvent.get()
                        .getOriginalEvent()
                        .get(CveSummaryEvent.LAST_KNOWN_CVE_DBID)
                .equals("CVE-2020-2")
        );

    }

    @Test
    public void findLastStoredNewsEvent() {
        //given: two stored events
        eventStore.append(new NewsServiceEvent(NewsServiceEvent.NewsEventType.CREATE,
                "123", new Date(), Collections.emptySet()));
        eventStore.append(new NewsServiceEvent(NewsServiceEvent.NewsEventType.SAVE,
                "456", new Date(), Collections.emptySet()));

        // when: the newest stored event is requested:
        var storedEvent = eventStore.findNewestEvent(NewsServiceEvent.KEY);

        //then:
        assertTrue("The stored event is retrieved.", storedEvent.isPresent());
        assertTrue("The type is correct.", storedEvent.get().getType().equals(NewsServiceEvent.KEY));
        assertTrue("The id is correct.",
                storedEvent.get().getOriginalEvent()
                .get(NewsServiceEvent.ARTICLE_ID).equals("456")
        );
    }
}
