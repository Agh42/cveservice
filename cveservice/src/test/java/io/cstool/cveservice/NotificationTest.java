package io.cstool.cveservice;

import io.cstool.cveservice.account.entity.Account;
import io.cstool.cveservice.account.entity.Inventory;
import io.cstool.cveservice.account.entity.Tenant;
import io.cstool.cveservice.infrastructure.database.entity.Cpe;
import io.cstool.cveservice.newsservice.repository.ArticleRepository;
import io.cstool.cveservice.notification.NotificationProducer;
import io.cstool.cveservice.notification.TemplateProcessor;
import io.cstool.cveservice.notification.entity.Notification;
import io.cstool.cveservice.subscription.entity.Subscription;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.test.util.AssertionErrors.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertTrue;

//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"dev", "test"})
@Slf4j
public class NotificationTest {

    @Autowired
    NotificationProducer notificationProducer;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    TemplateProcessor templateProcessor;

    @Test
    public void buildHotTopicsNotification() {
        var account = new Account("userId");
        account.setEmail("guybrush@melee.example");
        var from = Date.from(Instant.parse("2020-01-01T00:00:00Z"));

        var news = articleRepository.findByDatePublishedAfter(from);
        var notification = notificationProducer.newArticleNotificationFor(account, news, from);
        String html = templateProcessor.processTemplate(notification);
        assertTrue("Template or content was not found.",
                html.matches("(?s).*CSTOOL.*guybrush.*2020.*CVE-.*"));
    }

    @Test
    public void buildInventoriesNotification() {
        var subscription = Subscription.builder()
                .name("subs")
                .maxInventories(10)
                .maxProductsPerInventory(10)
                .build();
        var account = Tenant.newTenant("tenant", subscription)
                .createBossAccount("userId", "guybrush@melee.example");
        account.saveInventories(Set.of(
                Inventory.builder() // matching inventory, notifications ON
                    .name("inv1")
                    .products(List.of(
                        new Cpe("cpe:2.3:a:kernel:linux_kernel:2.6.21_rc4:*:x64:*:*:*:*:*", "Linux", true),
                        new Cpe("cpe:2.3:o:microsoft:windows_7:*:*:*:*:*:*:*:*", "Windows", true),
                        new Cpe("cpe:2.3:o:apple:iphone_os:5.0.1:-:ipodtouch:*:*:*:*:*", "iPhoneOS", true)))
                    .notify(true)
                        .build(),
                Inventory.builder() // matching inventory, notifications OFF
                        .name("inv2")
                        .products(List.of(
                                new Cpe("cpe:2.3:a:apple:safari:3.2.2b:-:windows:*:*:*:*:*", "Safari", true),
                                new Cpe("cpe:2.3:o:apple:iphone_os:5.0.1:-:ipodtouch:*:*:*:*:*", "iPhoneOS", true)))
                        .notify(false)
                        .build(),
                Inventory.builder() // differing inventory
                        .name("inv3")
                        .products(List.of(
                                new Cpe("cpe:2.3:a:kernel:linux_kernel:2.6.21_rc4:*:x64:*:*:*:*:*", "Linux", true),
                                new Cpe("cpe:2.3:o:microsoft:windows_7:*:*:*:*:*:*:*:*", "Windows", true)))
                        .notify(true)
                        .build()
        ));
        var from = Date.from(Instant.parse("2020-12-30T00:00:00Z"));
        var newCvesWithProducts = notificationProducer.getNewCvesWithProducts(from);

        var notification = notificationProducer.newCVEsNotificationFor(account, newCvesWithProducts);
        var html = templateProcessor.processTemplate(notification);
        assertTrue("Template or content was not found",
                html.matches("(?s).*CSTOOL.*CVE-2020.*inv1.*inv2.*"));
        assertFalse("Template contained surplus content.",
                html.matches("(?s).*inv3.*"));
    }
}